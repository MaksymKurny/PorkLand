local brain = require "brains/birdbrain"
require "stategraphs/SGbird"

local function ShouldSleep(inst)
	return DefaultSleepTest(inst) and not inst.sg:HasStateTag("flying")
end

local function OnDropped(inst)
	-- if IsOnWater(inst) then 				-- Need add
    --     inst.sg:GoToState("flyaway")
	-- else
		inst.sg:GoToState("stunned")
	-- end
end

local BIRD_TAGS = { "bird" }
local function OnAttacked(inst, data)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 30, BIRD_TAGS)
	local num_friends = 0
	local maxnum = 5
	for k, v in pairs(ents) do
		if v ~= inst then
			v:PushEvent("gohome")
			num_friends = num_friends + 1
		end

		if num_friends > maxnum then
			break
		end

	end
end


local function OnPutInInventory(inst)
    --Otherwise sleeper won't work if we're in a busy state
    inst.sg:GoToState("idle")
end


local function ChooseSeeds()
    -- return not TheWorld.state.iswinter and "seeds" or nil
    return "seeds"
end

local function SpawnPrefabChooser(inst)
    if inst.prefab == "kingfisher" and math.random() < 0.1 then
        return "fish"
    --elseif inst.prefab == "parrot_blue" and TheWorld.state.iswinter then
    --    return
    --elseif inst.prefab == "pigeon" and TheWorld.state.iswinter then
    --    return
    end

    if TheWorld.state.cycles <= 3 then
        -- The item drop is for drop-in players, players from the start of the game have to forage like normal
        return ChooseSeeds()
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    local players = FindPlayersInRange(x, y, z, 20, true)

    -- Give item if only fresh players are nearby
    local oldestplayer = -1
    for i, player in ipairs(players) do
        if player.components.age ~= nil then
            local playerage = player.components.age:GetAgeInDays()
            if playerage >= 3 then
                return ChooseSeeds()
            elseif playerage > oldestplayer then
                oldestplayer = playerage
            end
        end
    end

    -- Lower chance for older players to get item
    return oldestplayer >= 0
        and math.random() < .35 - oldestplayer * .1
        and ChooseItem()
        or ChooseSeeds()
end


local function makebird(name, feathername, soundname)
	local featherpostfix = feathername or name

	local assets =
	{
		Asset("ANIM", "anim/crow.zip"),
		Asset("ANIM", "anim/"..name.."_build.zip"),
		Asset("SOUND", "sound/birds.fsb"),
	}

	local prefabs =
	{
		"seeds",
		"smallmeat",
		"cookedsmallmeat",
		"feather_"..featherpostfix,
		"feather_crow",
	}

	local soundbank = "dontstarve_DLC003"
	if type(soundname) == "table" then
		soundbank = soundname.bank
        soundname = soundname.name
	end

	local sounds =
	{
		takeoff = soundbank.."/creatures/"..soundname.."/takeoff",
		--takeoff2 = soundbank.."/creatures/"..soundname.."/takeoff",
		chirp = soundbank.."/creatures/"..soundname.."/chirp",
		flyin = "dontstarve/birds/flyin",
		land = soundbank.."/creatures/"..soundname.."/land",
	}

	local function OnTrapped(inst, data)
		if data and data.trapper and data.trapper.settrapsymbols then
			data.trapper.settrapsymbols(name.."_build")
		end
	end

	local function canbeattacked(inst, attacked)
		return not inst.sg:HasStateTag("flying")
	end

	local function fn()
		local inst = CreateEntity()
		
		local trans = inst.entity:AddTransform()
		local anim = inst.entity:AddAnimState()
		local sound = inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

		inst.sounds = sounds
		inst.entity:AddPhysics()
		inst.Transform:SetTwoFaced()
		local shadow = inst.entity:AddDynamicShadow()
		shadow:SetSize( 1, .75 )
		shadow:Enable(false)

		inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.GROUND)
		--inst.Physics:CollidesWith(COLLISION.INTWALL)
		inst.Physics:SetSphere(1)
		inst.Physics:SetMass(1)

		MakeInventoryFloatable(inst, "takeoff_diagonal_pre", "stunned_loop")

		MakePoisonableCharacter(inst, "crow_body")
		inst.components.poisonable.damge_per_interval = TUNING.POISON_DAMAGE_PER_INTERVAL*50

		inst:AddTag("bird")
		inst:AddTag(name)
		inst:AddTag("smallcreature")

		local featherloot = "feather_"..featherpostfix

		anim:SetBank("crow")
		anim:SetBuild(name.."_build")
		inst.trappedbuild = name.."_build"
		
		anim:PlayAnimation("idle")
		
		inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

		inst:AddComponent("inspectable")

		inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
		inst.components.locomotor:EnableGroundSpeedMultiplier(false)
		inst.components.locomotor:SetTriggersCreep(false)
		inst:SetStateGraph("SGbird")

		inst:AddComponent("lootdropper")
		inst.components.lootdropper:AddRandomLoot(featherloot, 1)
		inst.components.lootdropper:AddRandomLoot("smallmeat", 1)
		inst.components.lootdropper.numrandomloot = 1

		inst:AddComponent("occupier")

		inst:AddComponent("eater")
    	if name == "kingfisher" then
    		inst.components.eater.foodprefs = {"SEEDS","MEAT"}
    		inst.components.eater.ablefoods = {"SEEDS","MEAT"}
    	else
			inst.components.eater:SetDiet({ FOODTYPE.SEEDS }, { FOODTYPE.SEEDS })
		end

		inst:AddComponent("sleeper")
		inst.components.sleeper:SetSleepTest(ShouldSleep)

		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.nobounce = true
		inst.components.inventoryitem.canbepickedup = false
        inst.components.inventoryitem.canbepickedupalive = true
		--inst.components.inventoryitem:SetOnDroppedFn(ondrop) -- done in MakeFeedablePet

		inst:AddComponent("cookable")
		inst.components.cookable.product = "cookedsmallmeat"
		
		inst:AddComponent("health")
		inst.components.health:SetMaxHealth(TUNING.BIRD_HEALTH)
		inst.components.health.murdersound = "dontstarve/wilson/hit_animal"

		inst.flyawaydistance = TUNING.BIRD_SEE_THREAT_DISTANCE

		if TheNet:GetServerGameMode() ~= "quagmire" then
			inst:AddComponent("combat")
			inst.components.combat.hiteffectsymbol = "crow_body"
			inst.components.combat.canbeattackedfn = canbeattacked
			
			MakeSmallBurnableCharacter(inst, "crow_body")
			MakeTinyFreezableCharacter(inst, "crow_body")
		end
		
		inst:AddComponent("hauntable")
        inst.components.hauntable:SetHauntValue(TUNING.HAUNT_TINY)
        inst.components.hauntable.panicable = true
		
		inst:SetBrain(brain)

		if not GetGameModeProperty("disable_bird_mercy_items") then
			inst:AddComponent("periodicspawner")
			inst.components.periodicspawner:SetPrefab(SpawnPrefabChooser)
			inst.components.periodicspawner:SetDensityInRange(20, 2)
			inst.components.periodicspawner:SetMinimumSpacing(8)
			--inst.components.periodicspawner:SetSpawnTestFn(seedspawntest)
		end

		-- inst.TrackInSpawner = TrackInSpawner

		inst:ListenForEvent("ontrapped", OnTrapped)
		inst:ListenForEvent("attacked", OnAttacked)
		-- inst:ListenForEvent("onremove", StopTrackingInSpawner)
		-- inst:ListenForEvent("enterlimbo", StopTrackingInSpawner)

		local birdspawner = TheWorld.components.birdspawner
        if birdspawner ~= nil then
            inst:ListenForEvent("onremove", birdspawner.StopTrackingFn)
            inst:ListenForEvent("enterlimbo", birdspawner.StopTrackingFn)
            -- inst:ListenForEvent("exitlimbo", birdspawner.StartTrackingFn)
            birdspawner:StartTracking(inst)
        end
		
		MakeFeedableSmallLivestock(inst, TUNING.BIRD_PERISH_TIME, OnPutInInventory, OnDropped)

		return inst
	end

	return Prefab(name, fn, assets, prefabs)
end


return makebird("pigeon", "robin_winter", "pigeon"),
	   makebird("parrot_blue", "robin_winter", {bank = "dontstarve_DLC002", name = "parrot"}),
	   makebird("kingfisher", "robin_winter", "king_fisher")
