require "brains/snakebrain"
require "stategraphs/SGsnake"

local trace = function() end

local assets=
{
	--Asset("ANIM", "anim/snake_build.zip"),
	--Asset("ANIM", "anim/snake_yellow_build.zip"),
	Asset("ANIM", "anim/snake_basic.zip"),
	Asset("ANIM", "anim/snake_water.zip"),	
	Asset("ANIM", "anim/snake_scaly_build.zip"),	
	Asset("SOUND", "sound/hound.fsb"),
}

local prefabs =
{
	"monstermeat",
	"snakeskin",
	"venomgland",
	-- "snakeoil",
}

local sounds = {
	default = {
		idle = "dontstarve_DLC002/creatures/snake/idle",
		pre_attack = "dontstarve_DLC002/creatures/snake/pre-attack",
		attack = "dontstarve_DLC002/creatures/snake/attack",
		hurt = "dontstarve_DLC002/creatures/snake/hurt",
		taunt = "dontstarve_DLC002/creatures/snake/taunt",
		death = "dontstarve_DLC002/creatures/snake/death",
		sleep = "dontstarve_DLC002/creatures/snake/sleep",
		move = "dontstarve_DLC002/creatures/snake/move",
	},
	amphibious = {
		idle = 			"dontstarve_DLC003/creatures/enemy/snake_amphibious/idle",
		pre_attack = 	"dontstarve_DLC002/creatures/enemy/snake_amphibious/pre-attack",
		attack = 		"dontstarve_DLC003/creatures/enemy/snake_amphibious/attack",
		hit = 			"dontstarve_DLC003/creatures/enemy/snake_amphibious/hit",
		taunt = 		"dontstarve_DLC002/creatures/enemy/snake_amphibious/taunt",
		death = 		"dontstarve_DLC003/creatures/enemy/snake_amphibious/death",
		sleep = 		"dontstarve_DLC002/creatures/enemy/snake_amphibious/sleep",
		move = 			"dontstarve_DLC002/creatures/enemy/snake_amphibious/move",	
	},
}


local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = {"FX", "NOCLICK","DECOR","INLIMBO"}

local function ShouldWakeUp(inst)
	return TheWorld.state.isnight
		or (inst.components.combat and inst.components.combat.target)
		or (inst.components.homeseeker and inst.components.homeseeker:HasHome() )
		or (inst.components.burnable and inst.components.burnable:IsBurning() )
		or (inst.components.follower and inst.components.follower.leader)
end

local function ShouldSleep(inst)
	return TheWorld.state.isday
		and not (inst.components.combat and inst.components.combat.target)
		and not (inst.components.homeseeker and inst.components.homeseeker:HasHome() )
		and not (inst.components.burnable and inst.components.burnable:IsBurning() )
		and not (inst.components.follower and inst.components.follower.leader)
end

local function OnNewTarget(inst, data)
	if inst.components.sleeper:IsAsleep() then
		inst.components.sleeper:WakeUp()
	end
end


local function retargetfn(inst)
	local dist = TUNING.SNAKE_TARGET_DIST
	local notags = {"FX", "NOCLICK","INLIMBO", "wall", "snake", "structure", "aquatic", "snakefriend"}
	return FindEntity(inst, dist, function(guy)
		return  inst.components.combat:CanTarget(guy)
	end, nil, notags)
end

local function KeepTarget(inst, target)
	return inst.components.combat:CanTarget(target) and inst:GetDistanceSqToInst(target) <= (TUNING.SNAKE_KEEP_TARGET_DIST*TUNING.SNAKE_KEEP_TARGET_DIST) and not target:HasTag("aquatic")
end

local function OnAttacked(inst, data)
	if data.attacker == nil or data.attacker:HasTag("jungletree") then return end
	inst.components.combat:SetTarget(data.attacker)
	inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("snake")and not dude.components.health:IsDead() end, 5)
end

local function OnAttackOther(inst, data)
	if data.attacker == nil then return end
	inst.components.combat:ShareTarget(data.target, SHARE_TARGET_DIST, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 5)
end

local function DoReturn(inst)
	--print("DoReturn", inst)
	if inst.components.homeseeker then
		inst.components.homeseeker:ForceGoHome()
	end
end

local function OnDay(inst, isday)
	--print("OnNight", inst)
	if isday and inst:IsAsleep() then
		DoReturn(inst)
	end
end


local function OnEntitySleep(inst)
	--print("OnEntitySleep", inst)
	if TheWorld.state.isday then
		DoReturn(inst)
	end
end

local function SanityAura(inst, observer)
    return observer.prefab == "webber" and 0 or -TUNING.SANITYAURA_SMALL
end

local function OnWaterChange(inst)
	local onwater = inst.components.amphibiouscreature.in_water
	if onwater then
        inst.sg:GoToState("submerge")
        inst.DynamicShadow:Enable(false)
    else  
		inst.sg:GoToState("emerge")
        inst.DynamicShadow:Enable(true)
    end
end

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddPhysics()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.Transform:SetFourFaced()
	
	inst:AddTag("scarytoprey")
	inst:AddTag("monster")
	inst:AddTag("hostile")
	inst:AddTag("snake")
	inst:AddTag("animal")
	inst:AddTag("canbetrapped")					

	MakeCharacterPhysics(inst, 10, .5)

	inst.AnimState:SetBank("snake")
	inst.AnimState:SetBuild("snake_build")
	inst.AnimState:PlayAnimation("idle")
	inst.AnimState:SetRayTestOnBB(true)
	
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("knownlocations")

	inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.runspeed = TUNING.SNAKE_SPEED

	inst:SetStateGraph("SGsnake")

	local brain = require "brains/snakebrain"
	inst:SetBrain(brain)

	inst:AddComponent("follower")

	inst:AddComponent("eater")
	-- inst.components.eater:SetCarnivore()
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
	inst.components.eater:SetCanEatHorrible()
	inst.components.eater:SetStrongStomach(true)


	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)
	inst.components.health.poison_damage_scale = 0 -- immune to poison

	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(TUNING.SNAKE_DAMAGE)
	inst.components.combat:SetAttackPeriod(TUNING.SNAKE_ATTACK_PERIOD)
	inst.components.combat:SetRetargetFunction(3, retargetfn)
	inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/snake/hurt")
	inst.components.combat:SetRange(2,3)

	inst:AddComponent("lootdropper")
	inst.components.lootdropper:AddRandomLoot("monstermeat", 1.00)
	inst.components.lootdropper:AddRandomLoot("snakeskin", 0.50)
	inst.components.lootdropper:AddRandomLoot("snakeoil", 0.01)
	inst.components.lootdropper.numrandomloot = math.random(0,1)

	inst:AddComponent("inspectable")
	
	MakeHauntablePanic(inst)

	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aurafn = SanityAura

	inst:AddComponent("sleeper")
	inst.components.sleeper:SetNocturnal(true)

	inst:ListenForEvent("newcombattarget", OnNewTarget)

	inst:WatchWorldState("isday", OnDay)
	
	inst.OnEntitySleep = OnEntitySleep

	inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("onattackother", OnAttackOther)

	MakeMediumFreezableCharacter(inst, "hound_body")

	return inst
end

local function amphibiousfn(Sim)
	local inst = fn(Sim)

	inst.entity:AddDynamicShadow()
	
	inst:AddTag("amphibious")
	inst:AddTag("snake_amphibious")
    inst:AddTag("breederpredator")		
	
	MakeAmphibiousCharacterPhysics(inst, 1, .5)
	
	inst.AnimState:SetBuild("snake_scaly_build")
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst:AddComponent("amphibiouscreature")
	inst.components.amphibiouscreature:SetBanks("snake_scaly", "snake_scaly_water")
    inst.components.amphibiouscreature:SetEnterWaterFn(OnWaterChange)
    inst.components.amphibiouscreature:SetExitWaterFn(OnWaterChange)
	
	inst.components.locomotor:CanPathfindOnWater()

	inst.sounds = sounds.amphibious
	
	MakeMediumBurnableCharacter(inst, "hound_body")

	return inst
end

return Prefab("snake_amphibious", amphibiousfn, assets, prefabs)