require "stategraphs/SGroc"
require "stategraphs/SGroc_head"
require "stategraphs/SGroc_leg"
require "stategraphs/SGroc_tail"

local assets=
{
	Asset("ANIM", "anim/roc_shadow.zip"),
	
	Asset("ANIM", "anim/roc_tail.zip"),
	
	Asset("ANIM", "anim/roc_leg.zip"),
	
	Asset("ANIM", "anim/roc_head_build.zip"),
	Asset("ANIM", "anim/roc_head_basic.zip"),
	Asset("ANIM", "anim/roc_head_actions.zip"),
	Asset("ANIM", "anim/roc_head_attacks.zip"),
}

local prefabs =
{
	"roc_leg",
	"roc_head",
	"roc_tail",
}

local function setstage(inst,stage)
	if stage == 1 then
		inst.Transform:SetScale(0.35,0.35,0.35)
        inst.components.locomotor.runspeed = 5
	elseif stage == 2 then
		inst.Transform:SetScale(0.65,0.65,0.65)
        inst.components.locomotor.runspeed = 7.5
    else
		inst.Transform:SetScale(1,1,1)
        inst.components.locomotor.runspeed = 10
	end
end

local function scalefn(inst,scale)
	inst.components.locomotor.runspeed = TUNING.ROC_SPEED * scale
	inst.components.shadowcaster:setrange(TUNING.ROC_SHADOWRANGE*scale)	
end

local function OnRemoved(inst)
	if TheWorld.components.rocmanager then
		TheWorld.components.rocmanager:RemoveRoc(inst)
	end
end

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddPhysics()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
	
	MakeNoPhysics(inst, 10, 1.5)
	inst.Physics:CollidesWith(COLLISION.GROUND)

	inst.Transform:SetScale(1.5,1.5,1.5)

	inst:AddTag("roc")
	inst:AddTag("roc_body")
	inst:AddTag("canopytracker")
	inst:AddTag("noteleport")
	inst:AddTag("windspeedimmune")
	inst:AddTag("NOCLICK")

	inst.AnimState:SetBank("roc")
	inst.AnimState:SetBuild("roc_shadow")
	inst.AnimState:PlayAnimation("ground_loop")

	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(1)	

	inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
		return inst
    end
	
	inst:AddComponent("colourtweener")
	inst.components.colourtweener:StartTween({1,1,1,(not TheWorld.state.isnight and not inst:HasTag("under_leaf_canopy")) and 0.5 or 0}, 3)
	
	inst:WatchWorldState("isday", function()
		if not inst:HasTag("under_leaf_canopy") then  
			inst.components.colourtweener:StartTween({1,1,1,0.5}, 3)		
		end
	end)

	inst:WatchWorldState("isnight", function()
		inst.components.colourtweener:StartTween({1,1,1,0}, 3)
	end)

	inst:AddComponent("knownlocations")

	inst:AddComponent("shadowcaster")

	inst:AddComponent("areaaware")

    inst:ListenForEvent("onremove", OnRemoved)

    inst:ListenForEvent("onchangecanopyzone", function()
    --[[
	    local ground = GetWorld()
	    local x,y,z = inst.Transform:GetWorldPosition()
	    local tile_type = ground.Map:GetTileAtPoint(x,y,z)
	    if tile_type == GROUND.DEEPRAINFOREST or tile_type == GROUND.GASJUNGLE or tile_type == GROUND.PIGRUINS then 
	        inst:AddTag("under_leaf_canopy")
	    else
	        inst:RemoveTag("under_leaf_canopy")
	    end 
	]]

	    if inst:HasTag("under_leaf_canopy") then
	    	inst.components.colourtweener:StartTween({1,1,1,0}, 1)
	    else
	    	if not TheWorld.state.isnight then
	    		inst.components.colourtweener:StartTween({1,1,1,0.5}, 1)
	    	end
	    end
    end, TheWorld)

	inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.runspeed = TUNING.ROC_SPEED

	inst:AddComponent("roccontroller")	
	inst.components.roccontroller:Setup(TUNING.ROC_SPEED, 0.35, 3)
	inst.components.roccontroller:Start()
	inst.components.roccontroller.scalefn = scalefn

	inst:SetStateGraph("SGroc")

	-- inst:AddComponent("health")
	-- inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)
	-- inst.components.health.poison_damage_scale = 0 -- immune to poison

	--inst:ListenForEvent("attacked", OnAttacked)
	--inst:ListenForEvent("onattackother", OnAttackOther)
	inst.setstage = setstage

	return inst
end

local function CreateParts(name, bank, build, anim, ground)	
	local function part_fn()
		local inst = CreateEntity()
		
		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddPhysics()
		inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()
		
		if name == "roc_tail" then
			inst.entity:AddDynamicShadow()
			inst.DynamicShadow:SetSize(8, 4)
		end
		
		if name == "roc_head" then
			inst.Transform:SetEightFaced()
			inst.Transform:SetScale(.8,.8,.8)
		else
			inst.Transform:SetEightFaced()
		end

		inst:AddTag("scarytoprey")
		inst:AddTag("monster")
		inst:AddTag("hostile")
		inst:AddTag("roc")
		inst:AddTag(name)
		inst:AddTag("noteleport")

		if name == "roc_leg" then
			MakeObstaclePhysics(inst, 2)
		end
		
		inst.AnimState:SetBank(bank)
		inst.AnimState:SetBuild(build)
		inst.AnimState:PlayAnimation(anim)
		-- inst.AnimState:SetRayTestOnBB(true)
		
		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		inst:AddComponent("knownlocations")

		-- inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
		-- inst.components.locomotor.runspeed = TUNING.SNAKE_SPEED

		inst:SetStateGraph("SG"..name)
		
		if ground then
			inst:AddComponent("groundpounder")	
			inst.components.groundpounder.destroyer = true
			inst.components.groundpounder.damageRings = 2
			inst.components.groundpounder.destructionRings = 1
			inst.components.groundpounder.numRings = name == "roc_head" and 3 or 2
		end
		
		inst:AddComponent("combat")
		inst.components.combat:SetDefaultDamage(1000)
		inst:AddComponent("inspectable")

		inst:AddComponent("sanityaura")
		inst.components.sanityaura.aurafn = function() return -TUNING.SANITYAURA_LARGE end

		-- inst:AddComponent("health")
		-- inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)
		-- inst.components.health.poison_damage_scale = 0 -- immune to poison

		-- inst:ListenForEvent("attacked", OnAttacked)
		-- inst:ListenForEvent("onattackother", OnAttackOther)

		return inst
	end

	return Prefab(name, part_fn, assets)
end
return Prefab("roc", fn, assets, prefabs),
	CreateParts("roc_head","head","roc_head_build","idle_loop", true),
	CreateParts("roc_leg", "foot", "roc_leg", "stomp_loop", true),
	CreateParts("roc_tail", "tail", "roc_tail", "tail_loop", false)