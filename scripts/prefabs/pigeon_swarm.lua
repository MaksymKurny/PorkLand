require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/crow.zip"),
    Asset("ANIM", "anim/pigeon_build.zip"),
}

local prefabs = 
{
    "pigeon" 
}

local function spawn_pigeon(inst)
    local DIST = 8
    local pigeon = SpawnPrefab("pigeon")                
    local x,y,z = inst.Transform:GetWorldPosition()
    x = x + (math.random()*DIST) - DIST/2
    z = z + (math.random()*DIST) - DIST/2
    pigeon.Transform:SetPosition(x,15,z)

    if math.random() < .5 then
       pigeon.Transform:SetRotation(180)
    end
end

local function set_spawn(inst)
    inst.pigeons = inst.pigeons - 1
    spawn_pigeon(inst)
    if inst.pigeons > 0 then
        inst:DoTaskInTime(math.random()*0.7,function()                
            set_spawn(inst)               
        end)    
    else
        inst:Remove()
    end
end

local function StopTrackingInSpawner(inst)
	if TheWorld and TheWorld.components.birdspawner then
		TheWorld.components.birdspawner:StopTracking(inst)
	end
end

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddPhysics()
	inst.entity:AddNetwork()

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

    inst.pigeons = math.random(3,7)

    set_spawn(inst)

	inst:ListenForEvent("onremove", StopTrackingInSpawner)

    return inst
end

return Prefab("pigeon_swarm", fn, assets, prefabs)