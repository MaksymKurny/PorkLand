local assets =
{
	Asset("ANIM", "anim/fountain_pillar.zip"),      
}

local function onload(inst, data)
    if data.rotation then
        inst.Transform:SetRotation(data.rotation)
    end
    if inst.components.workable and inst.components.workable.workleft < 1 then
        inst.AnimState:PushAnimation("pillar_collapsed",true)
    end
end

local function loadpostpass(inst,ents, data)
    if inst.components.workable.workleft <= 0 then
         inst.AnimState:PlayAnimation("pillar_collapsed")
    end
end

SetSharedLootTable('pugalisk_pillar',
{
    {'rocks',   1.00},
    {'rocks',   1.00},
    {'rocks',   1.00},
    {'rocks',   0.25},
    {'rocks',   0.25},
    {'relic_2', 0.05},
})

local function fn(Sim)
    local inst = CreateEntity()
	
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddMiniMapEntity()
	inst.entity:AddNetwork()
	
	inst.MiniMapEntity:SetIcon("pig_ruins_pillar.tex")

    MakeObstaclePhysics(inst, 0.5)
    inst.Transform:SetEightFaced()

    inst.AnimState:SetBuild("fountain_pillar")
    inst.AnimState:SetBank("fountain_pillar")
    inst.AnimState:PlayAnimation("pillar",true)

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

    inst:AddComponent("inspectable")
	
	inst:AddComponent("lootdropper") 
    inst.components.lootdropper:SetChanceLootTable('pugalisk_pillar')

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.MINE)
    inst.components.workable:SetWorkLeft(TUNING.PUGALISK_RUINS_PILLAR_WORK)
    inst.components.workable.savestate = true          
    inst.components.workable:SetOnFinishCallback(function(inst,worker)
		inst.SoundEmitter:PlaySound("dontstarve/common/destroy_stone")            
		inst.AnimState:PlayAnimation("pillar_collapse")
		inst.AnimState:PushAnimation("pillar_collapsed")
		inst.components.lootdropper:DropLoot(inst:GetPosition())
		--inst:Remove()
	end)

    inst:DoTaskInTime(0, function()
		local ent = TheSim:FindFirstEntityWithTag("pugalisk_fountain")
		if ent then
			inst.Transform:SetRotation(inst:GetAngleToPoint(ent:GetPosition()))
		end
	end)

	inst.LoadPostPass = loadpostpass
	
    return inst
end

return Prefab("pugalisk_ruins_pillar", fn, assets)