local assets =
{
	Asset("ANIM", "anim/lily_pad.zip"),
	Asset("ANIM", "anim/splash.zip"),
	Asset("MINIMAP_IMAGE", "lily_pad"),

}

local prefabs =
{
	"frog_poison",
	"mosquito",
}

function MakeLilypadPhysics(inst, rad)
    inst:AddTag("blocker")
    inst.entity:AddPhysics()
    --this is lame. Bullet wants 0 mass for static objects, 
    -- for for some reason it is slow when we do that

    -- Doesnt seem to slow anything down now.
    inst.Physics:SetMass(0)
    inst.Physics:SetCapsule(rad,0.01)
   -- inst.Physics:SetCylinder(rad, 1.0)
    inst.Physics:SetCollisionGroup(COLLISION.OBSTACLES)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
	inst.Physics:CollidesWith((TheWorld.has_ocean and COLLISION.GROUND) or COLLISION.WORLD) -- i ADDED THIS
    -- inst.Physics:CollidesWith(COLLISION.WAVES) TODO Fix This
    -- inst.Physics:CollidesWith(COLLISION.INTWALL) TODO fix this
	inst.Physics:CollidesWith(COLLISION.WORLD)
end

local function ReturnChildren(inst)
	for k, child in pairs(inst.components.childspawner.childrenoutside) do
		if child.components.homeseeker then
			child.components.homeseeker:GoHome()
		end
		child:PushEvent("gohome")
	end
end

local function OnSpawned(inst, child)
	if inst.components.childspawner.childname == "frog_poison" then
	 	inst.SoundEmitter:PlaySound("dontstarve_DLC003/movement/water/small_submerge")		
		child.sg:GoToState("submerge")
	end
end

local function refreshimage(inst)
	inst.AnimState:PlayAnimation(inst.size.."_idle", true)
	inst.Transform:SetRotation(inst.rotation)

	if inst.size == "small" then
		MakeLilypadPhysics(inst, 2)
	elseif inst.size == "med" then
		MakeLilypadPhysics(inst, 3)
	elseif inst.size == "big" then
		MakeLilypadPhysics(inst, 4.2)
	end
end

local function dayfn(inst)
	if inst.components.childspawner.childname == "frog_poison" then		
		inst.components.childspawner:StartSpawning()					
	end
		if inst.components.childspawner.childname == "mosquito" then
		inst.components.childspawner:StopSpawning()    		
	    ReturnChildren(inst)			
	end
end

local function duskfn(inst)
	 if inst.components.childspawner.childname == "mosquito" then
		inst.components.childspawner:StartSpawning()
    end
end

local function nightfn(inst)
	if inst.components.childspawner.childname == "frog_poison" then
		inst.components.childspawner:StopSpawning()    		
	    ReturnChildren(inst)	
    end		
end

local function OnInit(inst)
    inst.task = nil
    inst:WatchWorldState("isday", dayfn)
	inst:WatchWorldState("isdusk", duskfn)
	inst:WatchWorldState("isnight", nightfn)
	
	if TheWorld.state.isnight then nightfn(inst) end
	if TheWorld.state.isday   then dayfn(inst)   end
	if TheWorld.state.isdusk  then duskfn(inst)  end
end

local function OnPreLoad(inst, data)
    WorldSettings_ChildSpawner_PreLoad(inst, data, TUNING.POND_SPAWN_TIME, TUNING.POND_REGEN_TIME)
end

local function onload(inst, data, newents)
	if data then
		if data.size then
			inst.size = data.size
		end
		if data.childname then
			inst.components.childspawner.childname = data.childname 
		end
	end

	if TheWorld.state.isnight then nightfn(inst) end
	if TheWorld.state.isday   then dayfn(inst)   end
	if TheWorld.state.isdusk  then duskfn(inst)  end

	refreshimage(inst)
end

local function onsave(inst, data)
	data.size= inst.size
	data.rotation = inst.rotation
	data.childname = inst.components.childspawner.childname
	--OnSnowCoverChange(inst)
end

local function fn(pondtype)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	
	local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon("lily_pad.tex")
  --  MakeObstaclePhysics( inst, 1.95)

    anim:SetBuild("lily_pad")
    anim:SetBank("lily_pad")
	inst.size = "small"

    if math.random() < 0.66 then
    	if math.random() < 0.33 then
			inst.size = "med"
    	else
    		inst.size = "big"
    	end
    end

    inst.rotation = math.random(360)
    refreshimage(inst)

	anim:SetOrientation(ANIM_ORIENTATION.OnGround)
	anim:SetLayer(LAYER_BACKGROUND)
	anim:SetSortOrder(3)

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("waveobstacle")

	inst:AddComponent("childspawner")
	inst.components.childspawner:SetSpawnPeriod(TUNING.POND_SPAWN_TIME)
    if math.random() < 0.5 then
    	inst.components.childspawner.childname = "mosquito"
    	inst.components.childspawner:SetRegenPeriod(TUNING.MOSQUITO_REGEN_TIME)
    	inst.components.childspawner:SetMaxChildren(TUNING.MOSQUITO_MAX_SPAWN)
		WorldSettings_ChildSpawner_RegenPeriod(inst, TUNING.MOSQUITO_REGEN_TIME, TUNING.LILYPAD_ENABLED)
    else
		inst.components.childspawner.childname = "frog_poison"
		inst.components.childspawner:SetRegenPeriod(TUNING.FROG_POISON_REGEN_TIME)
		inst.components.childspawner:SetMaxChildren(TUNING.FROG_POISON_MAX_SPAWN)
		WorldSettings_ChildSpawner_RegenPeriod(inst, TUNING.FROG_POISON_REGEN_TIME, TUNING.LILYPAD_ENABLED)
	end
	WorldSettings_ChildSpawner_SpawnPeriod(inst, TUNING.POND_SPAWN_TIME, TUNING.LILYPAD_ENABLED)
	if not TUNING.LILYPAD_ENABLED then
        inst.components.childspawner.childreninside = 0
    end
	inst.components.childspawner:SetSpawnedFn(OnSpawned)
	inst.components.childspawner:StartRegen()
	inst.components.childspawner.allowwater = true
    inst.components.childspawner.allowboats = true
	
	inst.task = inst:DoTaskInTime(0, OnInit)
	
	inst.frozen = false

    inst:AddComponent("inspectable")
    inst.no_wet_prefix = true

	inst.OnLoad = onload
	inst.OnSave = onsave
	inst.OnPreLoad = OnPreLoad

	return inst
end

return Prefab("lilypad", fn, assets, prefabs)