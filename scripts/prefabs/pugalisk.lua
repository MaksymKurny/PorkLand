local head_brain = require("brains/pugalisk_headbrain")
local tail_brain = require("brains/pugalisk_tailbrain")

local pu = require ("prefabs/pugalisk_util")

local assets =
{	
    Asset("ANIM", "anim/python.zip"),
    Asset("ANIM", "anim/python_test.zip"),
    Asset("ANIM", "anim/python_segment_broken02_build.zip"),    
    Asset("ANIM", "anim/python_segment_broken_build.zip"),    
    Asset("ANIM", "anim/python_segment_build.zip"),                            
    Asset("ANIM", "anim/python_segment_tail02_build.zip"), 
    Asset("ANIM", "anim/python_segment_tail_build.zip"),

    Asset("MINIMAP_IMAGE", "snake_skull_buried"),
}

local prefabs =
{
    "snake_bone",
    "monstermeat",
    "gaze_beam",
    "pugalisk_body",
    "pugalisk_skull",
}

SetSharedLootTable( 'pugalisk', --Uhh... is this even used?? -Half
{
    {'monstermeat',             1.00},
    {'monstermeat',             1.00},
    {'monstermeat',             1.00},
})

SetSharedLootTable( 'pugalisk_corpse',
{
    {'pugalisk_skull',          1.00},
})
 
local SHAKE_DIST = 40

local function RedirectHealth(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    local originalinst = inst

    if inst.startpt then
        inst = inst.startpt
    end
	
    if amount < 0 and( (inst.components.segmented and inst.components.segmented.vulnerablesegments == 0) or inst:HasTag("tail") or inst:HasTag("head") ) then
        print("SAY", cause, afflicter.prefab)
		if cause == afflicter.prefab and afflicter.components.talker then
            --if afflicter ~= nil and afflicter:HasTag("player") then
			--	afflicter:DoTaskInTime(1, afflicter.PushEvent, "ANNOUNCE_PUGALISK_INVULNERABLE") -- for wisecracker
			--end
			afflicter.components.talker:Say(GetString(afflicter, "ANNOUNCE_PUGALISK_INVULNERABLE"))   -- TODO: Use wisecracker -Half    
        end
        inst.SoundEmitter:PlaySound("dontstarve/common/destroy_metal", nil, .25)
        inst.SoundEmitter:PlaySound("dontstarve/wilson/hit_metal")

        return true
    elseif amount and inst.host then

        local fx = SpawnPrefab("snake_scales_fx")
        fx.Transform:SetScale(1.5, 1.5, 1.5)
        local pt = inst:GetPosition()
        fx.Transform:SetPosition(pt.x, pt.y + 2 + math.random()*2, pt.z)

        inst:PushEvent("dohitanim")
        -- inst.host.components.health:DoDelta(amount, overtime, cause, ignore_invincible, true) -- No longer needed -Half
        inst.host:PushEvent("attacked")
        return false
    end    
    return true
end

local PYTHON_MUST_TAGS = { "_combat", "_health" }
local PYTHON_CANT_TAGS = { "FX", "NOCLICK", "INLIMBO", "pugalisk" }
local PYTHON_ONEOF_TAGS = { "character", "player", "animal", "monster"}

local function Retarget(inst, target_dist)  
    return FindEntity(inst, target_dist, function(guy)
		print(guy)
        return inst.components.combat:CanTarget(guy)
    end, PYTHON_MUST_TAGS, PYTHON_CANT_TAGS, PYTHON_ONEOF_TAGS)
end

local function OnHit(inst, attacker)    
    local host = inst
    if inst.host then
        host = inst.host      
    end    

    if attacker and (not inst.target or inst.target:HasTag("player")) then
        host.target = attacker 
        host.components.combat:SetTarget(attacker)
    end
    
end

local function segmentfn(Sim)
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Transform:SetScale(1.5 , 1.5, 1.5)
    inst.Transform:SetEightFaced()

    inst.AnimState:SetFinalOffset( -10 )
    
    inst.AnimState:SetBank("giant_snake")
    inst.AnimState:SetBuild("python_test")
    inst.AnimState:PlayAnimation("test_segment")

    inst:AddTag("pugalisk")
    inst:AddTag("groundpoundimmune")
    inst:AddTag("noteleport") -- TODO: not in DST, use teleport override cmp
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(0)
    --inst.components.combat.hiteffectsymbol = "test_segments"-- "wormmovefx" -- TODO: Maybe remove these dumb test names? -Half
    inst.components.combat.onhitfn = OnHit    

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(9999)
    inst.components.health.destroytime = 5
    inst.components.health.redirect = RedirectHealth

    inst:AddComponent("inspectable")
    inst.components.inspectable.nameoverride = "pugalisk"  
    inst.name = STRINGS.NAMES.PUGALISK  -- TODO: Does this even do anything in DST?

    inst:AddComponent("lootdropper")
    inst.components.lootdropper.lootdropangle = 360 --TODO: Not in DST
    inst.components.lootdropper.speed = 3 + (math.random()*3) --TODO: Not in DST

    inst.AnimState:Hide("broken01")
    inst.AnimState:Hide("broken02")

    inst.persists = false

    return inst
end

--======================================================================

local function Body_SegmentOnDeath(inst)

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/explode")

    local pt = Vector3(inst.Transform:GetWorldPosition())

    inst.components.lootdropper:SpawnLootPrefab("snake_bone",pt)
       
    if math.random() < 0.6 then
        inst.components.lootdropper:SpawnLootPrefab("boneshard",pt)
    end        
    if math.random() < 0.2 then
        inst.components.lootdropper:SpawnLootPrefab("monstermeat",pt)
    end
    if math.random() < 0.005 then
        inst.components.lootdropper:SpawnLootPrefab("redgem", pt)
    end
    if math.random() < 0.005 then
        inst.components.lootdropper:SpawnLootPrefab("bluegem", pt)
    end
    if math.random() < 0.05 then
        inst.components.lootdropper:SpawnLootPrefab("spoiled_fish", pt)
    end
    
    local fx = SpawnPrefab("snake_scales_fx")    
    fx.Transform:SetScale(1.5, 1.5, 1.5)
    fx.Transform:SetPosition(pt.x, pt.y + 2 + math.random()*2, pt.z)
end

local function Body_OnBodyComplete(inst)
    if inst.exitpt then
        inst.exitpt.AnimState:SetBank("giant_snake")
        inst.exitpt.AnimState:SetBuild("python_test")
        inst.exitpt.AnimState:PlayAnimation("dirt_static")  

		ShakeAllCameras(CAMERASHAKE.VERTICAL, 0.5, 0.03, 2, inst, SHAKE_DIST)	

        inst.exitpt.Physics:SetActive(true)
        inst.exitpt.components.groundpounder:GroundPound()   

        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/emerge","emerge")
        inst.SoundEmitter:SetParameter( "emerge", "start", math.random() )

        if inst.host then   
            inst.host:PushEvent("bodycomplete",{ pos=Vector3(inst.exitpt.Transform:GetWorldPosition()), angle = inst.angle })                            
        end                                     
    end
end

local function Body_OnBodyFinished(inst)
    if inst.host then  
        inst.host:PushEvent("bodyfinished",{ body=inst })                                                
    end                      
    inst:Remove()
end

local function bodyfn(Sim)

    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Transform:SetSixFaced()
    
    inst.Transform:SetScale(1.5, 1.5, 1.5)
    
    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("giant_snake") 
    inst.AnimState:SetBuild("python_test")
    inst.AnimState:PlayAnimation("dirt_static")

    inst.AnimState:Hide("broken01")
    inst.AnimState:Hide("broken02")

    inst.AnimState:SetFinalOffset( 0 )

    inst.name = STRINGS.NAMES.PUGALISK
    inst.invulnerable = true

    ------------------------------------------

    inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("pugalisk")
    inst:AddTag("scarytoprey")
    inst:AddTag("largecreature")
    inst:AddTag("groundpoundimmune")
    inst:AddTag("noteleport") -- TODO: not in DST, use teleport override cmp

    ------------------
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
    ------------------
    
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(9999)
    inst.components.health.destroytime = 5
    inst.components.health.redirect = RedirectHealth

    ------------------

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.PUGALISK_DAMAGE)
    inst.components.combat.playerdamagepercent = 0.75

    inst.components.combat.hiteffectsymbol = "hit_target"
    inst.components.combat:SetOnHit(OnHit)
    
    ------------------------------------------

    inst:AddComponent("lootdropper")

    ------------------------------------------

    inst:AddComponent("inspectable")
    inst.components.inspectable.nameoverride = "pugalisk"
    ------------------------------------------

    inst:AddComponent("knownlocations")

    ------------------------------------------

    inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 2
    inst.components.groundpounder.destructionRings = 1
    inst.components.groundpounder.numRings = 2
    inst.components.groundpounder.groundpounddamagemult = 30/TUNING.PUGALISK_DAMAGE
    inst.components.groundpounder.groundpoundfx= "groundpound_nosound_fx"

    ------------------------------------------

    inst:AddComponent("segmented")
    inst.components.segmented.segment_deathfn = Body_SegmentOnDeath

    inst:ListenForEvent("bodycomplete", Body_OnBodyComplete) 

    inst:ListenForEvent("bodyfinished", Body_OnBodyFinished)

    inst.persists = false

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/movement_LP", "speed")
    inst.SoundEmitter:SetParameter("speed", "intensity", 0)

    ------------------------------------------

    return inst
end

--===========================================================

local function Tail_Retarget(inst)
    return Retarget(inst, TUNING.PUGALISK_TAIL_TARGET_DIST)
end

local function tailfn(Sim)

    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Transform:SetSixFaced()
    
    inst.Transform:SetScale(1.5, 1.5, 1.5)
    
    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("giant_snake")
    inst.AnimState:SetBuild("python_test") 
    inst.AnimState:PlayAnimation("tail_idle_loop", true)

    inst.AnimState:Hide("broken01")
    inst.AnimState:Hide("broken02")

    inst.AnimState:SetFinalOffset( 0 )

    inst.name = STRINGS.NAMES.PUGALISK
    inst.invulnerable = true

    ------------------------------------------

    inst:AddTag("tail")
    inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("pugalisk")
    inst:AddTag("scarytoprey")
    inst:AddTag("largecreature")
    inst:AddTag("groundpoundimmune")
    inst:AddTag("noteleport")

    ------------------------------------------
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
    ------------------------------------------
    
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(9999)
    inst.components.health.destroytime = 5
    inst.components.health.redirect = RedirectHealth

    ------------------------------------------  

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.PUGALISK_DAMAGE/2)
    inst.components.combat.playerdamagepercent = 0.5
    inst.components.combat:SetRange(TUNING.PUGALISK_MELEE_RANGE, TUNING.PUGALISK_MELEE_RANGE)
    inst.components.combat.hiteffectsymbol = "hit_target" -- "wormmovefx"
    inst.components.combat:SetAttackPeriod(TUNING.PUGALISK_ATTACK_PERIOD/2)
    inst.components.combat:SetRetargetFunction(0.5, Tail_Retarget)
    inst.components.combat:SetOnHit(OnHit)

    ------------------------------------------

    inst:AddComponent("locomotor")

    ------------------------------------------

    inst:AddComponent("lootdropper")

    ------------------------------------------

    inst:AddComponent("inspectable")
    inst.components.inspectable.nameoverride = "pugalisk"

    ------------------------------------------

    inst.persists = false

    ------------------------------------------
    inst:SetStateGraph("SGpugalisk_head")

    inst:SetBrain(tail_brain)    

    return inst
end

--===========================================================

local function CalcSanityAura(inst, observer)
    return -TUNING.SANITYAURA_LARGE
end

local function OnDeath(inst)
    -- TODO: Needs a rewrite for multiplayer -Half

    -- TheCamera:Shake("FULL",3, 0.05, .2)
    -- local mb = inst.components.multibody
    -- for i,body in ipairs(mb.bodies)do
    --     body.components.health:Kill()
    -- end
    -- if mb.tail then
    --     mb.tail.components.health:Kill()
    -- end 

    if inst.home and inst.home.reactivate then
        inst.home.reactivate(inst.home)
    end    
    -- mb:Kill()

    local ent = TheSim:FindFirstEntityWithTag("pugalisk_trap_door")
    if ent and ent.reactivate then
        ent.reactivate(ent)
    end
end

local function OnSave(inst, data)
    local refs = {} 
    if inst.home then
        data.home = inst.home.GUID
        table.insert(refs, inst.home.GUID)
    end
    return refs
end

local function OnLoadPostPass(inst, newents, data)
    if data and data.home then
        local home = newents[data.home].entity
        if home then
            print("FOUND HOME, RELOADING IT")
            inst.home = home
        end
    end
end

local function Head_OnBodyComplete(inst, data)
    local pt = pu.findsafelocation( data.pos , data.angle/DEGREES )
    inst.Transform:SetPosition(pt.x,0,pt.z)
    inst:DoTaskInTime(0.75, function() 

        --local player = GetClosestInstWithTag("player", inst, SHAKE_DIST) --TODO: This is messy and inefficent -Half
        --if player then                                   
        --    player.components.playercontroller:ShakeCamera(inst, "VERTICAL", 0.3, 0.03, 1, SHAKE_DIST)
        --end        
		ShakeAllCameras(CAMERASHAKE.VERTICAL, 0.3, 0.03, 1, inst, SHAKE_DIST)		
        inst.components.groundpounder:GroundPound()
        
        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/emerge","emerge")
        inst.SoundEmitter:SetParameter( "emerge", "start", math.random() )
        
        pu.DetermineAction(inst)
    end)
end

local function Head_OnBodyFinished(inst, data)
    inst.components.multibody:RemoveBody(data.body)
end

local function Head_Retarget(inst)  
    return Retarget(inst, TUNING.PUGALISK_TARGET_DIST)
end

local function fn(Sim)
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Transform:SetSixFaced()

    MakeObstaclePhysics(inst, 1)

    inst.Transform:SetScale(1.5, 1.5, 1.5)

    inst.AnimState:SetBank("giant_snake")
    inst.AnimState:SetBuild("python_test") --"python"
    inst.AnimState:PushAnimation("head_idle_loop", true)

    inst.AnimState:SetFinalOffset( 0 )

    ------------------------------------------

    inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("pugalisk")
    inst:AddTag("scarytoprey")
    inst:AddTag("largecreature")
    inst:AddTag("groundpoundimmune")    
    inst:AddTag("head")    
    inst:AddTag("noflinch")
    inst:AddTag("noteleport")

    ------------------------------------------  
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
    ------------------------------------------  

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura

    ------------------------------------------   

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.PUGALISK_DAMAGE)
    inst.components.combat.playerdamagepercent = 0.75
    inst.components.combat:SetRange(TUNING.BEARGER_ATTACK_RANGE, TUNING.PUGALISK_MELEE_RANGE)
    inst.components.combat.hiteffectsymbol = "hit_target" -- "wormmovefx"
    inst.components.combat:SetAttackPeriod(TUNING.PUGALISK_ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(0.5, Head_Retarget)
    inst.components.combat:SetOnHit(OnHit)

    ------------------------------------------

    inst:AddComponent("lootdropper")
    
    ------------------------------------------       

    inst:AddComponent("inspectable")
    inst.components.inspectable.nameoverride = "pugalisk"
    inst.name = STRINGS.NAMES.PUGALISK
    
    ------------------------------------------

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.PUGALISK_HEALTH)
    inst.components.health.destroytime = 5
    inst.components.health:StartRegen(1, 2)
    inst.components.health.redirect = RedirectHealth

    ------------------------------------------

    inst:AddComponent("knownlocations")
    
    ------------------------------------------
    
    inst:AddComponent("locomotor")

    ------------------------------------------

    inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 2
    inst.components.groundpounder.destructionRings = 1
    inst.components.groundpounder.numRings = 2
    inst.components.groundpounder.groundpounddamagemult = 30/TUNING.PUGALISK_DAMAGE
    inst.components.groundpounder.groundpoundfx = "groundpound_nosound_fx"

    ------------------------------------------
    
    inst:AddComponent("multibody")    
    inst.components.multibody:Setup(5, "pugalisk_body")   
    
    ------------------------------------------    

    inst:ListenForEvent("bodycomplete", Head_OnBodyComplete)     
    
    inst:ListenForEvent("bodyfinished", Head_OnBodyFinished)

    inst:ListenForEvent("death", OnDeath)

    inst.spawntask = inst:DoTaskInTime(0,function() -- TODO: This is messy -Half
            inst.spawned = true
        end)
    
    inst:SetStateGraph("SGpugalisk_head")

    inst:SetBrain(head_brain)

    inst.OnSave = OnSave 
    inst.OnLoadPostPass = OnLoadPostPass

    return inst
end

--===========================================================

local function OnFinishedCallback(inst, worker)

    inst.MiniMapEntity:SetEnabled(false)
    inst:RemoveComponent("workable")

    SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())

    if worker then
        -- TODO: Needs a rewrite for multiplayer -Half
        -- figure out which side to drop the loot
        -- local pt = Vector3(inst.Transform:GetWorldPosition())
        -- local hispos = Vector3(worker.Transform:GetWorldPosition())

        -- local he_right = ((hispos - pt):Dot(TheCamera:GetRightVec()) > 0)
        
        -- if he_right then
        --     inst.components.lootdropper:DropLoot(pt - (TheCamera:GetRightVec()*(math.random()+1)))           
        -- else
        --     inst.components.lootdropper:DropLoot(pt + (TheCamera:GetRightVec()*(math.random()+1)))            
        -- end        
        
        inst:Remove()
    end 
end


local function corpsefn(Sim)

    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("snake_skull_buried.tex")

    inst.Transform:SetSixFaced()
    
    inst.Transform:SetScale(1.5, 1.5, 1.5)
    
    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("giant_snake")
    inst.AnimState:SetBuild("python_test") 
    inst.AnimState:PlayAnimation("death_idle", true)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end

    inst:AddComponent("lootdropper")

    ------------------------------------------

    inst:AddComponent("inspectable")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("pugalisk_corpse")
    inst.components.workable:SetOnFinishCallback(OnFinishedCallback)

    return inst
end

return  Prefab("pugalisk", fn, assets, prefabs),
        Prefab("pugalisk_body", bodyfn, assets, prefabs),
        Prefab("pugalisk_tail", tailfn, assets, prefabs),
        Prefab("pugalisk_segment", segmentfn, assets, prefabs),
        Prefab("pugalisk_corpse", corpsefn, assets, prefabs)