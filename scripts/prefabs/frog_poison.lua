require "brains/frogbrain"
require "stategraphs/SGfrog"

local poisonassets=
{
	Asset("ANIM", "anim/frog.zip"),
	Asset("ANIM", "anim/frog_water.zip"),	
	Asset("ANIM", "anim/frog_treefrog_build.zip"),
	Asset("SOUND", "sound/frog.fsb"),
}

local prefabs =
{
	"froglegs",
	"splash",
	"venomgland",
	"froglegs_poison",
}

local sounds = {
	poison = {
		grunt = "dontstarve_DLC003/creatures/enemy/frog_poison/grunt",
		walk =  "dontstarve/frog/walk",
		spit =  "dontstarve_DLC003/creatures/enemy/frog_poison/attack_spit",
		voice = "dontstarve_DLC003/creatures/enemy/frog_poison/attack_spit",		
		splat = "dontstarve/frog/splat",
		die =   "dontstarve_DLC003/creatures/enemy/frog_poison/death",
		wake =  "dontstarve/frog/wake",
	},	
}
local RESTARGET_MUST_TAGS = {"_combat","_health"}
local RETARGET_CANT_TAGS = {"merm"}
local function retargetpoisonfrogfn(inst)
    if not inst.components.health:IsDead() and not inst.components.sleeper:IsAsleep() then
        return FindEntity(inst, TUNING.FROG_TARGET_DIST, function(guy)
            if not guy.components.health:IsDead() and not guy:HasTag("hippopotamoose") then
                return guy.components.inventory ~= nil or guy:HasTag("insect") 
            end
        end,
        RESTARGET_MUST_TAGS, -- see entityreplica.lua
        RETARGET_CANT_TAGS
        )
    end
end

local function ShouldSleep(inst)
    if inst.components.knownlocations:GetLocation("home") ~= nil or inst:HasTag("aporkalypse_cleanup") then
        return false
    end

    -- Homeless frogs will sleep at night.
    return TheWorld.state.isnight
end

local function OnAttacked(inst, data)
	inst.components.combat:SetTarget(data.attacker)
	inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("frog") and not dude.components.health:IsDead() end, 5)
end

local function OnGoingHome(inst)
	local fx = SpawnPrefab("splash")
	local pos = inst:GetPosition()
	fx.Transform:SetPosition(pos.x, pos.y, pos.z)

	--local splash = PlayFX(Vector3(inst.Transform:GetWorldPosition() ), "splash", "splash", "splash")
	inst.SoundEmitter:PlaySound("dontstarve/frog/splash")
end
local function OnSave(inst, data)
    if inst:HasTag("aporkalypse_cleanup") then
        data.aporkalypse_cleanup = true
    end
end

local function OnLoad(inst, data) 
	if data and data.aporkalypse_cleanup then
		inst:AddTag("aporkalypse_cleanup")
	end
end

local function OnWaterChangeCommon(inst)
	local onwater = inst.components.amphibiouscreature.in_water
	--TODO add the other speed changes
	if onwater then
		inst.DynamicShadow:Enable(false)
		inst.components.locomotor.walkspeed = 3
	else
		inst.DynamicShadow:Enable(true)
		inst.components.locomotor.walkspeed = 4
	end
	if not inst.sg:HasStateTag("falling") then
		local noanim = inst:GetTimeAlive() < 1
		inst.sg:GoToState(onwater and "submerge" or "emerge", noanim)
	end
end

local function poisonfn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddPhysics()
	inst.entity:AddSoundEmitter()
	inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()
	
	inst.DynamicShadow:SetSize(1.5, .75)
	inst.Transform:SetFourFaced()
	
	MakeAmphibiousCharacterPhysics(inst, 1, .3)
	MakeCharacterPhysics(inst, 1, .3)

	inst:AddTag("animal")
	inst:AddTag("prey")
    inst:AddTag("hostile")
	inst:AddTag("smallcreature")
	inst:AddTag("frog")
	inst:AddTag("canbetrapped")    
	inst:AddTag("duskok")
	inst:AddTag("eatsbait")
	inst:AddTag("scarytoprey")
	
	inst.AnimState:SetBank("frog")
	inst.AnimState:SetBuild("frog_treefrog_build")
	inst.AnimState:PlayAnimation("idle")

    inst.entity:SetPristine()

	if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("thief")
	inst:AddComponent("eater")
	inst:AddComponent("knownlocations")
	inst:AddComponent("inspectable")

	inst:AddComponent("sleeper")
	inst.components.sleeper:SetSleepTest(ShouldSleep)
	
	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(TUNING.FROG_HEALTH)

	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"froglegs_poison"})
	inst.components.lootdropper:AddRandomLoot("venomgland", 0.5)
	
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(TUNING.FROG_DAMAGE)
	inst.components.combat:SetAttackPeriod(TUNING.FROG_ATTACK_PERIOD)
	inst.components.combat:SetRetargetFunction(3, retargetpoisonfrogfn)
	inst.components.combat.onhitotherfn = function(inst, other, damage) inst.components.thief:StealItem(other) end
	
	inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = 4
	inst.components.locomotor.runspeed = 8
	
	inst:AddComponent("amphibiouscreature")
	inst.components.amphibiouscreature:SetBanks("frog", "frog_water")
    inst.components.amphibiouscreature:SetEnterWaterFn(OnWaterChangeCommon)
    inst.components.amphibiouscreature:SetExitWaterFn(OnWaterChangeCommon)
	
	inst.components.locomotor:CanPathfindOnWater()

	inst:SetStateGraph("SGfrog")

	local brain = require "brains/frogbrain"
	inst:SetBrain(brain)
	
	MakeTinyFreezableCharacter(inst, "frogsack")
	MakeSmallBurnableCharacter(inst, "frogsack")

	inst.sounds = sounds.poison
	
	inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("goinghome", OnGoingHome)
	
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad 
	
	return inst
end

return Prefab("frog_poison", poisonfn, poisonassets, prefabs)