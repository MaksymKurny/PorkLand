local assets =
{
	Asset("ANIM", "anim/blunderbuss.zip"),
	Asset("ANIM", "anim/swap_blunderbuss.zip"),
    Asset("ANIM", "anim/swap_blunderbuss_loaded.zip"),
    Asset("ANIM", "anim/blunderbuss_ammo.zip"),

    Asset("INV_IMAGE", "blunderbuss"),
    Asset("INV_IMAGE", "blunderbuss_loaded"),
}

local function onequip(inst, owner, force)
    owner.AnimState:OverrideSymbol("swap_object", inst.override_bank, "swap_blunderbuss")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal") 
end

local function onunequip(inst,owner) 
    owner.AnimState:ClearOverrideSymbol("swap_object")
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function CanTakeAmmo(inst, ammo, giver)
    return ammo.prefab == "gunpowder"
end

local function OnTakeAmmo(inst, data)
	local ammo = data and data.item
    if not ammo then return end

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/items/weapon/blunderbuss_load")

    inst.components.trader.enabled = false
    --Set up as projectile thrower instead of crummy bat
    inst:AddTag("blunderbuss")
    inst:AddTag("projectile")
	inst:AddTag("rangedweapon")
    inst.components.weapon:SetProjectile("gunpowder_projectile")
    --Change ranges
    inst.components.weapon:SetRange(TUNING.BLUNDERBUSS_ATTACK_RANGE, TUNING.BLUNDERBUSS_HIT_RANGE)
    inst.components.weapon:SetDamage(TUNING.GUNPOWDER_DAMAGE)
	
	--Change equip overrides
    inst.override_bank = "swap_blunderbuss_loaded"

    --If equipped, change current equip overrides
	local owner = inst.components.equippable:IsEquipped() and inst.components.inventoryitem.owner or nil
    if owner then
        owner.AnimState:OverrideSymbol("swap_object", inst.override_bank, "swap_blunderbuss")
    end

    --Change invo image.
    inst.components.inventoryitem:ChangeImageName("blunderbuss_loaded")
end

local function OnLoseAmmo(inst, data)
    inst.components.trader.enabled = true
    --Go back to crummy bat mode
    inst:RemoveTag("blunderbuss")
    inst:RemoveTag("projectile")
	inst:RemoveTag("rangedweapon")
    inst.components.weapon:SetProjectile(nil)

    --Change ranges back to melee
    inst.components.weapon:SetRange(nil, nil)
    inst.components.weapon:SetDamage(TUNING.UNARMED_DAMAGE)

    --Change equip overrides
    inst.override_bank = "swap_blunderbuss"

    --If equipped, change current equip overrides
    local owner = inst.components.equippable:IsEquipped() and inst.components.inventoryitem.owner or nil
    if owner then
        owner.AnimState:OverrideSymbol("swap_object", inst.override_bank, "swap_blunderbuss")
    end

    inst.components.inventoryitem:ChangeImageName("blunderbuss")
end

local function OnAttack(inst, attacker, target)
    local removed_item = inst.components.inventory:GetItemInSlot(1)
    if removed_item then
        removed_item:Remove()
    end
end

local function fn()
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst)
	
    inst:AddTag("blunderbus")

    anim:SetBank("blunderbuss")
    anim:SetBuild("blunderbuss")
    anim:PlayAnimation("idle")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("weapon")
    inst.components.weapon.projectilelaunchsymbol = "swap_object"
    inst.components.weapon:SetOnProjectileLaunched(OnAttack)

    inst:AddComponent("inventoryitem")

    inst:AddComponent("inventory")
    inst.components.inventory.maxslots = 1
    inst:ListenForEvent("itemlose", OnLoseAmmo)
    inst:ListenForEvent("itemget", OnTakeAmmo)

    inst:AddComponent("trader")
    inst.components.trader.deleteitemonaccept = false
    inst.components.trader:SetAcceptTest(CanTakeAmmo)
    inst.components.trader.enabled = true
    inst.components.trader.acceptnontradable = true
    
    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst.override_bank = "swap_blunderbuss"

    return inst
end

local function OnHit(inst, attacker, target, weapon)
    local impactfx = SpawnPrefab("impact")
    if impactfx and attacker then
        local follower = impactfx.entity:AddFollower()
        follower:FollowSymbol(target.GUID, target.components.combat.hiteffectsymbol, 0, 0, 0)
        impactfx:FacePoint(attacker.Transform:GetWorldPosition())
    end
    inst:Remove()
end

local function projectile_fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("amo01")
    inst.AnimState:SetBuild("blunderbuss_ammo")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("projectile")
    --inst:AddTag("sharp")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("weapon")
	inst.components.weapon:SetDamage(TUNING.GUNPOWDER_DAMAGE)

    inst:AddComponent("projectile")
    inst.components.projectile:SetSpeed(50)
    inst.components.projectile:SetOnHitFn(OnHit)
	inst.components.projectile.has_damage_set = true

    inst.persists = false

    return inst
end

return Prefab("blunderbuss", fn, assets),
       Prefab("gunpowder_projectile", projectile_fn, assets) 
