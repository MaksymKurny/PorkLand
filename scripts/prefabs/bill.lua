require "stategraphs/SGbill"
local brain = require "brains/billbrain"

local assets =
{
	Asset("ANIM", "anim/bill_agro_build.zip"),
	Asset("ANIM", "anim/bill_calm_build.zip"),
	Asset("ANIM", "anim/bill_basic.zip"),
	Asset("ANIM", "anim/bill_water.zip"),
}

local prefabs =
{
	"bill_quill",
}

SetSharedLootTable( 'bill',
{
    {'meat',            1.00},
    {'bill_quill',      1.00},
    {'bill_quill',      1.00},
    {'bill_quill',      0.33},
})

function IsBillFood(item)
	return item:HasTag("billfood")
end

local function UpdateAggro(inst)
	inst.lotusTheifNearby = false
	local x, y, z = inst.Transform:GetWorldPosition()
	local players = FindPlayersInRange(x, y, z, TUNING.BILL_TARGET_DIST)
	for _, player in pairs(players) do
		if player.components.inventory:FindItem(IsBillFood) then
			inst.lotusTheifNearby = true
			break
		end
	end
	-- If the threat level changes then modify the build.
	inst.AnimState:SetBuild(inst.lotusTheifNearby and "bill_agro_build" or "bill_calm_build")
end

local function UpdateTumble(inst)
	inst.letsGetReadyToTumble = true
end

local function KeepTarget(inst, target)
    return inst.components.combat:CanTarget(target) and target:HasTag("player")
end

local function CanEat(inst, item)
	return item:HasTag("billfood")
end

local function OnAttacked(inst, data)
    local attacker = data and data.attacker
    inst.components.combat:SetTarget(attacker)
    inst.components.combat:ShareTarget(attacker, 20, function(dude) return dude:HasTag("platapine") end, 2)
end

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddPhysics()
	inst.entity:AddSoundEmitter()
	inst.entity:AddDynamicShadow()
	inst.entity:AddNetwork()

	inst.letsGetReadyToTumble = false

	inst.DynamicShadow:SetSize(1, 0.75)
	inst.Transform:SetFourFaced()

	MakeAmphibiousCharacterPhysics(inst, 1, 0.5)
	MakePoisonableCharacter(inst)

	inst:AddTag("scarytoprey")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("platapine")

	inst.AnimState:SetBank("bill")
	inst.AnimState:SetBuild("bill_calm_build")
	inst.AnimState:PlayAnimation("idle", true)

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("locomotor")
	inst.components.locomotor.runspeed = TUNING.BILL_RUN_SPEED

	inst:AddComponent("amphibiouscreature")
    inst.components.amphibiouscreature:SetBanks("bill", "bill_water")
	inst.components.amphibiouscreature:SetEnterWaterFn(function(inst)
		inst.landspeed = inst.components.locomotor.runspeed
		inst.components.locomotor.runspeed = TUNING.BILL_RUN_SPEED
		inst.hop_distance = inst.components.locomotor.hop_distance
		inst.components.locomotor.hop_distance = 4
	end)
	inst.components.amphibiouscreature:SetExitWaterFn(function(inst)
		if inst.landspeed then
			inst.components.locomotor.runspeed = inst.landspeed
		end
		if inst.hop_distance then
			inst.components.locomotor.hop_distance = inst.hop_distance
		end
	end)
	inst.components.locomotor:CanPathfindOnWater()

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('bill')

	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(TUNING.BILL_HEALTH)
	inst.components.health.murdersound = "dontstarve/rabbit/scream_short"

	inst:AddComponent("inspectable")
	inst:AddComponent("sleeper")
	inst:AddComponent("eater")
	inst.components.eater:SetPrefersEatingTag("billfood")

	inst:AddComponent("knownlocations")
	inst:DoTaskInTime(0, function() 
		inst.components.knownlocations:RememberLocation("home", inst:GetPosition()) 
	end)

	inst:DoPeriodicTask(1, UpdateAggro)
	inst:DoPeriodicTask(4, UpdateTumble)

	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(TUNING.BILL_DAMAGE)
	inst.components.combat:SetAttackPeriod(TUNING.BILL_ATTACK_PERIOD)
	inst.components.combat:SetRange(2, 3)
	inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat.hiteffectsymbol = "chest"

	MakeSmallBurnableCharacter(inst, "chest")
	MakeTinyFreezableCharacter(inst, "chest")

	inst:ListenForEvent("attacked", OnAttacked)

	inst:SetStateGraph("SGbill")
	inst:SetBrain(brain)

	return inst
end

return Prefab("bill", fn, assets, prefabs)