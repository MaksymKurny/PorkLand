local assets =
{
    Asset("ANIM", "anim/room_shelves.zip"),
    Asset("ANIM", "anim/pedestal_key.zip"),
    Asset("ANIM", "anim/pedestal_crate.zip"),
}

local prefabs =
{
    "shelf_slot",
}

local function setPlayerUncraftable(inst)
    inst:AddTag("playercrafted") 

    inst:RemoveTag("NOCLICK")
    inst:AddComponent("lootdropper")
    
    inst.entity:AddSoundEmitter()
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnWorkCallback(function(inst, worker, workleft)
		if workleft <= 0 then
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			if inst.SoundEmitter then
				inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			end

			if inst.shelves and #inst.shelves > 0 then
				for i, v in ipairs(inst.shelves)do           
					v.empty(v)
					v:Remove()
				end
			end

			inst:Remove()
		end
	end)
end

local function onBuilt(inst)
    setPlayerUncraftable(inst)
    inst.onbuilt = true         
end

local function SetImage(inst, ent, slot)
    local image = nil 

	if ent ~= nil and ent.components.inventoryitem ~= nil then
        image = #(ent.components.inventoryitem.imagename or "") > 0 and
            ent.components.inventoryitem.imagename or
            ent.prefab
    end
	
    if image ~= nil then
		if not inst:HasTag("playercrafted") and ent.components.perishable then 
			ent.components.perishable:StopPerishing() 
		end

		inst.AnimState:OverrideSymbol(slot, GetInventoryItemAtlas(image..".tex"), image..".tex")

        inst.imagename = ent or ""
    else
        inst.imagename = ""
        inst.AnimState:ClearOverrideSymbol(slot)
    end
end 

local function SetImageFromName(inst, name, slot)
    local image = name
	print("name", name)
    if image ~= nil then 
        local texname = image..".tex"

        inst.AnimState:OverrideSymbol(slot, GetInventoryItemAtlas(texname), texname)
        inst.imagename = image
    else
        inst.imagename = ""
        inst.AnimState:ClearOverrideSymbol(slot)
    end
end 

local function displaynamefn(inst)
    return "whatever"
end

local function spawnchildren(inst)
    if not inst.childrenspawned then
        inst.shelves = {}
        for i = 1, inst.size do
            local object = SpawnPrefab("shelf_slot")   

            if inst.swp_img_list and object.components.inventoryitem and object.components.shelfer then
                object.components.inventoryitem:PutOnShelf(inst, inst.swp_img_list[i])
                object.components.shelfer:SetShelf(inst, inst.swp_img_list[i])            
            else 
				if object.components.inventoryitem and object.components.shelfer then
                object.components.inventoryitem:PutOnShelf(inst,"SWAP_img"..i)
                object.components.shelfer:SetShelf(inst, "SWAP_img"..i)  
				end
            end
            table.insert(inst.shelves, object)
            if inst.shelfitems then

                for index,set in pairs(inst.shelfitems)do
                    if set[1] == i then
                        local item = SpawnPrefab(set[2])
                        if item and object.components.shelfer then
                            object.components.shelfer:AcceptGift(nil, item)
                        end
                    end
                end
            end
        end
        inst.childrenspawned = true
    end
end

local function unlock(inst, key, doer)
    inst.AnimState:Hide("LOCK")
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/royal_gallery/unlock") 
    if inst.shelves then
        for i,object in ipairs(inst.shelves) do 
			local item  = object.components.shelfer:GetGift()
			if item ~= nil then
            object.components.shelfer:Enable()
			end
        end 
    end
    inst:AddTag("NOCLICK")
	inst.islocked = true
end

local function lock(inst)
    inst.AnimState:Show("LOCK") 
    if inst.shelves then
        for i,object in ipairs(inst.shelves) do  
			if object.components.shelfer then
            object.components.shelfer:Disable()
			end
        end    
    end
end

local function onsave(inst, data)    
    if inst.childrenspawned then
        data.childrenspawned = inst.childrenspawned
    end
    data.rotation = inst.Transform:GetRotation()    
    if inst.onbuilt then
        data.onbuilt = inst.onbuilt
    end     
    if inst:HasTag("playercrafted") then
        data.playercrafted = true
    end    

    data.shelves = {}
    if inst.shelves then
        for i, v in ipairs(inst.shelves)do
            table.insert(data.shelves, v.GUID)
        end
    end	

    data.islocked = inst.islocked
	data.textura = inst.textura	
end

local function onload(inst, data)
	if data == nil then 
		return 
	end
	
    if data.rotation then
        inst.Transform:SetRotation(data.rotation)
    end    
	
    if data.childrenspawned then
        inst.childrenspawned = data.childrenspawned
    end
	
    if data.onbuilt then
        setPlayerUncraftable(inst)
        inst.onbuilt = data.onbuilt
    end  
	
    if data.playercrafted then
        inst:AddTag("playercrafted")
    end 
	
    if data.islocked then
        inst.islocked = data.islocked
    end
	
    if not inst.islocked then
        lock(inst)
    else
        unlock(inst,nil)
    end
	
	if data.textura then 
		inst.textura = data.textura 
	
		inst.AnimState:PlayAnimation(data.textura, true)
	end	

end

local function onloadpostpass(inst, ents, data)
--[[
    inst.shelves = {}
    if data and data.shelves and ents then
        for i, v in ipairs(data.shelves)do
		if ents and v and ents[v].entity then
            local shelfer = ents[v].entity
            if shelfer then
                table.insert(inst.shelves, shelfer)
            end
		end	
        end
    end
]]	
end  

local function docurse(inst)
    if math.random() < 0.3 then
        local ghost = SpawnPrefab("pigghost")
        local pt = Vector3(inst.Transform:GetWorldPosition())
        ghost.Transform:SetPosition(pt.x,pt.y,pt.z)
    end
end

local function common(setsize, swp_img_list, locked, physics_round)
    local inst = CreateEntity()
	
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddPhysics()
	inst.entity:AddNetwork()	

    if physics_round then
        MakeObstaclePhysics(inst, .5)
    else
        MakeInteriorPhysics(inst, 1.6, 1, 0.2)
    end 

--    inst.AnimState:SetOrientation(ANIM_ORIENTATION.RotatingBillboard)
	inst.Transform:SetTwoFaced()

    inst:AddTag("NOCLICK")
    inst:AddTag("wallsection")
    inst:AddTag("furniture")    

    inst.AnimState:SetBuild("room_shelves")
    inst.AnimState:SetBank("bookcase")
    inst.AnimState:PlayAnimation("wood", false)

    inst.Transform:SetRotation(-90)
	
	inst.entity:SetPristine()

   	if not TheWorld.ismastersim then
       	return inst
    end

    inst.imagename = nil 

    inst.SetImage = SetImage
    inst.SetImageFromName = SetImageFromName

    inst.swp_img_list = swp_img_list
    inst.size = setsize or 6
    if swp_img_list then
        for i=1, inst.size do
            SetImageFromName(inst, nil, swp_img_list[i])
        end
    else
        for i=1, inst.size do
            SetImageFromName(inst, nil, "SWAP_img"..i)
        end
    end
   
    inst:ListenForEvent( "onbuilt", function()
        onBuilt(inst)
    end)          

    inst.OnSave = onsave 
    inst.OnLoad = onload
    inst.OnLoadPostPass = onloadpostpass

    inst:DoTaskInTime(0, function() 
        if inst:HasTag("playercrafted") then
            setPlayerUncraftable(inst)
        end

        spawnchildren(inst, locked)
		
        if locked and not inst.islocked then
			lock(inst)
		else
			unlock(inst)
        end
    end)

    return inst
end
---------------------------------------------------------------------------
local function MakeShelve(name, layer, tags, build, bank, anim)
	local function basicshelve()
		local inst = common()
		
		inst.AnimState:PlayAnimation(anim or name, false)
		if tags ~= nil then
			for i, v in ipairs(tags) do
				inst:AddTag(v)
			end
		end
		local _layer = layer or true
		if _layer then
			inst.AnimState:SetLayer(LAYER_WORLD_BACKGROUND)
			inst.AnimState:SetSortOrder(3)
		end
		if build then
			inst.AnimState:SetBuild(build)
		end
		if bank then
			inst.AnimState:SetBank(bank)
		end
		
		return inst
	end
	return Prefab("shelves_"..name, basicshelve, assets, prefabs)
end
---------------------------------------------------------------------------
local function MakeQueenShelve(name, anim, item)
	local function queen_common()
		local inst = common(1, {"SWAP_SIGN"}, true, true)

		inst.AnimState:SetBuild("pedestal_crate")
		inst.AnimState:SetBank("pedestal")
		inst.AnimState:PlayAnimation(anim, false)
		
		if not TheWorld.ismastersim then
			return inst
		end
		
		inst.shelfitems = {{1, item}}
		
		inst:AddComponent("inspectable")
		inst.components.inspectable.nameoverride = "royal_gallery"
		inst.name = STRINGS.NAMES.ROYAL_GALLERY  
		   
		inst:RemoveTag("NOCLICK")

		inst:AddComponent("klaussacklock")
		inst.components.klaussacklock:SetOnUseKey(unlock)
		inst.klaussackkeyid = "royal gallery"	  
		
	  
		return inst
	end
	return Prefab(name, queen_common, assets, prefabs)
end
---------------------------------------------------------------------------
local function OnTimerDone(inst, data)
    if data.name == "spawndelay" then
    local pote = SpawnPrefab("shelves_ruins")
    pote.Transform:SetPosition(inst.Transform:GetWorldPosition())
	
    if inst.shelves and #inst.shelves > 0 then
        for i, v in ipairs(inst.shelves)do           
            v.empty(v)
            v:Remove()
        end
    end	
	inst:Remove()
    end
end

local function ruins()
    local inst = CreateEntity()
	
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddPhysics()	
	inst.entity:AddNetwork()
	
	local minimap = inst.entity:AddMiniMapEntity()
    inst.MiniMapEntity:SetIcon("shelf_ruins.tex")	

    MakeObstaclePhysics(inst, .5)

--    inst.AnimState:SetOrientation(ANIM_ORIENTATION.RotatingBillboard)
	inst.Transform:SetTwoFaced()

    inst:AddTag("NOCLICK")
    inst:AddTag("wallsection")
    inst:AddTag("furniture")  
    inst:AddTag("pigcurse")	

    inst.AnimState:SetBuild("room_shelves")
    inst.AnimState:SetBank("bookcase")    
    inst.AnimState:PlayAnimation("ruins", false) 
    inst.curse = docurse
	
	inst:AddTag("playercrafted")

    inst.Transform:SetRotation(-90)

	inst.entity:SetPristine()

   	if not TheWorld.ismastersim then
       	return inst
    end
	
	inst.imagename = nil 

    inst.SetImage = SetImage
    inst.SetImageFromName = SetImageFromName

    inst.swp_img_list = nil
    inst.size = 1

    for i=1, inst.size do
		SetImageFromName(inst, nil, "SWAP_img"..i)
    end
   
    inst:ListenForEvent( "onbuilt", function()
        onBuilt(inst)
    end)          

    inst.OnSave = onsave 
    inst.OnLoad = onload
    inst.OnLoadPostPass = onloadpostpass

    inst:DoTaskInTime(0, function() 
        if inst:HasTag("playercrafted") then
            setPlayerUncraftable(inst)
        end

        spawnchildren(inst,nil) 
        unlock(inst)
    end)

    inst:AddComponent("timer")
    inst:ListenForEvent("timerdone", OnTimerDone)

    return inst
end

local function key()    
    local inst = CreateEntity()
    
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddPhysics()	
    inst.entity:AddNetwork()
	
    MakeInventoryPhysics(inst)
    
    inst.AnimState:SetBank("pedestal_key")
    inst.AnimState:SetBuild("pedestal_key")
    inst.AnimState:PlayAnimation("idle")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end	
	
	inst:AddComponent("klaussackkey")
	inst.components.klaussackkey.keytype = "royal gallery"
    
    inst:AddComponent("inspectable")
    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("inventoryitem")

    inst:AddComponent("tradable")

    return inst
end

return  
	MakeShelve("wood"),
	MakeShelve("basic"),
	MakeShelve("marble"),
	MakeShelve("glass"),
	MakeShelve("ladder"),
	MakeShelve("hutch"),
	MakeShelve("industrial"),
	MakeShelve("adjustable"),
	MakeShelve("fridge"),
	MakeShelve("midcentury"),
	MakeShelve("wallmount"),
	MakeShelve("aframe"),
	MakeShelve("crates"),
	MakeShelve("pipe"),
	MakeShelve("hattree"),
	MakeShelve("pallet"),
	MakeShelve("floating"),
	MakeShelve("metal", 		nil, nil, nil, nil, "metalcrates"),
	MakeShelve("cinderblocks",  nil, {"playercrafted", "estante"}),
	MakeShelve("display", 		false, nil, "room_shelves", "bookcase", "displayshelf_wood"),
	MakeShelve("display_metal", false, nil, "room_shelves", "bookcase", "displayshelf_metal"),
			
	MakeQueenShelve("shelves_queen_display_1", "lock19_east", "key_to_city"),
	MakeQueenShelve("shelves_queen_display_2", "lock17_east", "trinket_giftshop_4"),
	MakeQueenShelve("shelves_queen_display_3", "lock12_west", "city_hammer"),
	MakeQueenShelve("shelves_queen_display_4", "lock12_west", "trinket_giftshop_3"),

	Prefab("shelves_ruins", ruins, assets, prefabs),
	Prefab("pedestal_key",key,assets,prefabs)