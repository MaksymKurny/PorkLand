local assets =
{
	Asset( "ANIM", "anim/cloud_puff_soft.zip" )
}

local function SetAnim(inst)
	inst.AnimState:PlayAnimation("poofanim", false)
	local x, y, z = inst.Transform:GetWorldPosition()

	local map = GetWorld().Map

	local tx, ty = map:GetTileXYAtPoint(x, y, z)

	local left = map:IsLand(map:GetTile(tx, ty)) and map:IsWater(map:GetTile(tx, ty))
end

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddNetwork()

    inst.AnimState:SetBuild("cloud_puff_soft")
    inst.AnimState:SetBank("splash_clouds_drop")
    inst.AnimState:PlayAnimation("idle_sink")

	inst:AddTag("FX")
	inst:AddTag("NOCLICK")

	inst.persists = false
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst:ListenForEvent("animover", inst.Remove)
	inst:ListenForEvent("entitysleep", inst.Remove)

    return inst
end

return Prefab("cloudpuff", fn, assets)