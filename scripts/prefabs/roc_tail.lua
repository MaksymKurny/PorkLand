require "stategraphs/SGroc_tail"

local assets=
{
	Asset("ANIM", "anim/roc_tail.zip"),
}

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddPhysics()
	inst.entity:AddSoundEmitter()
	inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

	inst.DynamicShadow:SetSize(8, 4)
	
	inst.Transform:SetSixFaced()

	inst:AddTag("scarytoprey")
	inst:AddTag("monster")
	inst:AddTag("hostile")
	inst:AddTag("roc")
	inst:AddTag("roc_tail")
	inst:AddTag("noteleport")

	-- MakeObstaclePhysics(inst, 2)

	inst.AnimState:SetBank("tail")
	inst.AnimState:SetBuild("roc_tail")
	inst.AnimState:PlayAnimation("tail_loop")
	-- inst.AnimState:SetRayTestOnBB(true)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
		return inst
    end

	inst:AddComponent("knownlocations")

	-- inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	-- inst.components.locomotor.runspeed = TUNING.SNAKE_SPEED

	inst:SetStateGraph("SGroc_tail")

	-- inst:AddComponent("health")
	-- inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)
	-- inst.components.health.poison_damage_scale = 0 -- immune to poison

	inst:AddComponent("groundpounder")	
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 2
    inst.components.groundpounder.destructionRings = 1
    inst.components.groundpounder.numRings = 2	

	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(1000)
	inst:AddComponent("inspectable")

	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aurafn = function() return -TUNING.SANITYAURA_LARGE end

	-- inst:ListenForEvent("attacked", OnAttacked)
	-- inst:ListenForEvent("onattackother", OnAttackOther)

	return inst
end

return Prefab("roc_tail", fn, assets)