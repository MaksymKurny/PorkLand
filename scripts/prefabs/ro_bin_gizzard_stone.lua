local assets=
{
    Asset("ANIM", "anim/ro_bin_gem.zip"),
    Asset("INV_IMAGE", "ro_bin_gem"),
    Asset("INV_IMAGE", "ro_bin_gem_closed"),
}

local SPAWN_DIST = 30

local function RebuildTile(inst)
    if inst.components.inventoryitem:IsHeld() then
        local owner = inst.components.inventoryitem.owner
        inst.components.inventoryitem:RemoveFromOwner(true)
        if owner.components.container then
            owner.components.container:GiveItem(inst)
        elseif owner.components.inventory then
            owner.components.inventory:GiveItem(inst)
        end
    end
end

local function GetSpawnPoint(pt)
    local theta = math.random() * 2 * PI
    local radius = SPAWN_DIST
	local offset = FindWalkableOffset(pt, theta, radius, 12, true)
	return offset ~= nil and (pt + offset) or nil
end

local function SpawnRoBin(inst, spawn_pt, spawnevent)
    -- print("ro_bin_gizzard_stone - SpawnRoBin")

    local pt = inst:GetPosition()
    -- print("    near", pt)
    local spawn_pt = spawn_pt or GetSpawnPoint(pt)
    if spawn_pt then
        -- print("    at", spawn_pt)
        local ro_bin = SpawnPrefab("ro_bin")
        if ro_bin then
            ro_bin.Physics:Teleport(spawn_pt:Get())
            ro_bin:FacePoint(pt:Get())

            if spawnevent then
                ro_bin:PushEvent("spawnin")
            end

            return ro_bin
        end

    else
        -- this is not fatal, they can try again in a new location by picking up the bone again
        -- print("ro_bin_gizzard_stone - SpawnRoBin: Couldn't find a suitable spawn point for ro_bin")
    end
end


local function StopRespawn(inst)
    -- print("ro_bin_gizzard_stone - StopRespawn")
    if inst.respawntask then
        inst.respawntask:Cancel()
        inst.respawntask = nil
        inst.respawntime = nil
    end
end

local function RebindRoBin(inst, ro_bin)
    ro_bin = ro_bin or TheSim:FindFirstEntityWithTag("ro_bin")
    if ro_bin then

        inst.AnimState:PlayAnimation("idle_loop", true)
        inst.components.inventoryitem:ChangeImageName(inst.openEye)
        inst:ListenForEvent("death", function() inst:OnRoBinDeath() end, ro_bin)

        if ro_bin.components.follower.leader ~= inst then
            ro_bin.components.follower:SetLeader(inst)
        end
        return true
    end
end

local function RespawnRoBin(inst)
    -- print("ro_bin_gizzard_stone - RespawnRoBin")

    StopRespawn(inst)
    RebindRoBin(inst, TheSim:FindFirstEntityWithTag("ro_bin") or SpawnRoBin(inst))
end

local function StartRespawn(inst, time)
    StopRespawn(inst)

    local respawntime = time or 0
    if respawntime then
        inst.respawntask = inst:DoTaskInTime(respawntime, RespawnRoBin)
        inst.respawntime = GetTime() + respawntime
        inst.AnimState:PlayAnimation("dead", true)
        inst.components.inventoryitem:ChangeImageName(inst.closedEye)
    end
end

local function OnRoBinDeath(inst)
    StartRespawn(inst, TUNING.CHESTER_RESPAWN_TIME)
end

local function FixRoBin(inst)
	inst.fixtask = nil
	--take an existing ro_bin if there is one
	if not RebindRoBin(inst) then
        inst.AnimState:PlayAnimation("dead", true)
        inst.components.inventoryitem:ChangeImageName(inst.closedEye)
		
		if inst.components.inventoryitem.owner then
			local time_remaining = inst.respawntime ~= nil and math.max(0, inst.respawntime - GetTime()) or 0
            StartRespawn(inst, time_remaining)
		end
	end
end

local function OnPutInInventory(inst)
	if inst.fixtask == nil then
		inst.fixtask = inst:DoTaskInTime(1, FixRoBin)
	end
end

local function OnSave(inst, data)
    -- print("ro_bin_eyebone - OnSave")
    if inst.respawntime ~= nil then
        local time = GetTime()
        if inst.respawntime > time then
            data.respawntimeremaining = inst.respawntime - time
        end
    end
    if inst.robinspawned then
        data.robinspawned = true
    end
end


local function OnLoad(inst, data)
	if data == nil then
        return
    end
	
	if data and data.respawntimeremaining then
		inst.respawntime = data.respawntimeremaining + GetTime()
	end
    if data and data.robinspawned then
        inst.robinspawned = data.robinspawned        
    end
end

local function GetStatus(inst)
    -- print("smallbird - GetStatus")
    return inst.respawntask ~= nil and "WAITING" or nil
end


local function fn()
    local inst = CreateEntity()
	
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    --inst:AddTag("ro_bin_eyebone")
    inst:AddTag("chester_eyebone") --This tag is used in save code and stuff, that's why I didn't change it
    inst:AddTag("ro_bin_gizzard_stone")
    
    inst:AddTag("irreplaceable")
    inst:AddTag("dropontravel")
	inst:AddTag("nonpotatable")
    inst:AddTag("follower_leash")

    MakeInventoryPhysics(inst)
    
    inst.AnimState:SetBank("ro_bin_gem")
    inst.AnimState:SetBuild("ro_bin_gem")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.openEye = "ro_bin_gem"
    inst.closedEye = "ro_bin_gem_closed"   
	
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetOnPutInInventoryFn(OnPutInInventory)
    inst.components.inventoryitem:ChangeImageName(inst.openEye)  
    
    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = GetStatus
	inst.components.inspectable:RecordViews()

    inst:AddComponent("leader")
	
	MakeHauntableLaunch(inst)

    inst.OnLoad = OnLoad
    inst.OnSave = OnSave
    inst.OnRoBinDeath = OnRoBinDeath

    inst.fixtask = inst:DoTaskInTime(0, function() 
        if not inst.robinspawned then
			inst.robinspawned = true       
			SpawnRoBin(inst, inst:GetPosition(), true)
        end
    end)

	inst.fixtask = inst:DoTaskInTime(1, FixRoBin)

    return inst
end

return Prefab("ro_bin_gizzard_stone", fn, assets)