local assets=
{
	Asset("ANIM", "anim/bonestaff.zip"),
	Asset("ANIM", "anim/swap_bonestaff.zip"), 
}

local prefabs = 
{
    "staffcastfx",
    "gaze_beam",
}


---------BONE STAFF------------
local function spawngaze(staff, target, pos)
    local owner = staff.components.inventoryitem.owner
    local mousepos = pos:Get()
    local rotation = nil

    --if TheInput:ControllerAttached() then
    --    local pc = owner.components.playercontroller
    --    if pc.reticule and pc.reticule.targetpos then
    --        local pt = pc.reticule.targetpos
    --        rotation = owner:GetAngleToPoint(pt.x, pt.y, pt.z)
    --    end        
    --    
    --    if not rotation then 
    --        print("GETTING PLAYER ROTATION")
    --        rotation = owner.Transform:GetRotation()
    --    end
    --    print(rotation, owner:GetAngleToPoint(pos:Get()) )
    --else
    --    
    --end
	rotation = owner:GetAngleToPoint(pos:Get())

    local beam = SpawnPrefab("gaze_beam")
    local pt = Vector3(owner.Transform:GetWorldPosition())
    local angle = rotation * DEGREES
    local radius = 4 
    local offset = Vector3(radius * math.cos( angle ), 0, -radius * math.sin( angle ))
    local newpt = pt+offset

    beam.Transform:SetPosition(newpt.x,newpt.y,newpt.z)
    beam.host = owner
    beam.Transform:SetRotation(rotation)
end

local function endbonecast(inst)
    if inst.gazetask then
        inst.gazetask:Cancel()
        inst.gazetask = nil
    end
    inst.SoundEmitter:KillSound("gazor")
end

local function creategaze(staff, target, pos)
    local caster = staff.components.inventoryitem.owner
    if staff.gazetask then
        endbonecast(inst)
    end
    if caster then
        staff.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/gaze_start")
        staff.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/gaze_LP","gazor")
		
        staff.gazetask = staff:DoPeriodicTask(0.4, function() 
			if caster.sg:HasStateTag("busy") then
				spawngaze(staff, target, pos)
			else
				endbonecast(staff)
			end
		end)
    end
end

local function cancreategaze(staff, caster, target, pos)
    return cancreatelight(staff, caster, target, pos)
end

---------COMMON FUNCTIONS---------

local function onfinished(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
    inst:Remove()
end

local function unimplementeditem(inst)
    local player = GetPlayer()
    player.components.talker:Say(GetString(player.prefab, "ANNOUNCE_UNIMPLEMENTED"))
    if player.components.health.currenthealth > 1 then
        player.components.health:DoDelta(-player.components.health.currenthealth * 0.5)
    end

    if inst.components.useableitem then
        inst.components.useableitem:StopUsingItem()
    end
end

local function bone_reticuletargetfn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(5, 0.001, 0)) -- raised this off the ground a touch so it wont have any z-fighting with the ground biome transition tiles.
end

local function onequip(inst, owner) 
	owner.AnimState:OverrideSymbol("swap_object", "swap_staffs", "swap_bonestaff")
	owner.AnimState:Show("ARM_carry") 
	owner.AnimState:Hide("ARM_normal") 
end

local function onunequip(inst, owner) 
	owner.AnimState:Hide("ARM_carry") 
	owner.AnimState:Show("ARM_normal") 
end

local function bonefn()
	local inst = CreateEntity()
	
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
	
    MakeInventoryPhysics(inst)
    
    anim:SetBank("staffs")
    anim:SetBuild("bonestaff")
    anim:PlayAnimation("bonestaff")
    -------   
	
	inst:AddTag("show_spoilage")
	inst:AddTag("nosteal")
	inst:AddTag("nopunch")
	
	local floater_swap_data =
    {
        sym_build = "swap_bonestaff",
        sym_name = "swap_bonestaff",
        bank = "staffs",
        anim = "bonestaff"
    }
    MakeInventoryFloatable(inst, "med", 0.1, {0.9, 0.4, 0.9}, true, -13, floater_swap_data)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("tradable")
    
    inst:AddComponent("inventoryitem")
    
    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)


    inst.fxcolour = {223/255, 208/255, 69/255}
   -- inst.castsound = "dontstarve_DLC003/creatures/boss/pugalisk/gaze_start"

    inst:AddComponent("spellcaster")
    inst.components.spellcaster:SetSpellFn(creategaze)
    --inst.components.spellcaster:SetSpellTestFn(cancreategaze)
    inst.components.spellcaster.canuseonpoint = true
    inst.components.spellcaster.canuseonpoint_water = true
    inst.components.spellcaster.canusefrominventory = false
    inst.castfast = true

    inst:AddComponent("perishable")
    inst.components.perishable:SetPerishTime(TUNING.PERISH_ONE_DAY/2)
    inst.components.perishable:StartPerishing()
    inst.components.perishable.onperishreplacement = "boneshard"

    inst.endcast = endbonecast

    inst:AddComponent("reticule")
    inst.components.reticule.ispassableatallpoints = true
    inst.components.reticule.ease = true
    inst.components.reticule.targetfn = bone_reticuletargetfn

    return inst
end

return Prefab("bonestaff", bonefn, assets, prefabs)
