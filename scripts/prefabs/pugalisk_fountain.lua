local assets =
{
    Asset("ANIM", "anim/python_fountain.zip"),      
}

local prefabs =
{
    "waterdrop",
    "lifeplant",
}

local function OnActivate(inst, doer)
    print("ACTIVATED THE PUGALISK FOUNTAIN")

    inst.AnimState:PlayAnimation("flow_pst")
    inst.AnimState:PushAnimation("off", true)
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/resurrection")
    inst.SoundEmitter:KillSound("burble")
    inst.components.activatable.inactive = false
    inst.dry = true

    local drop = SpawnPrefab("waterdrop")
    drop.fountain = inst
    doer.components.inventory:GiveItem(drop, nil, inst:GetPosition())

    local ent = TheSim:FindFirstEntityWithTag("pugalisk_trap_door")
    if ent then
        ent.activate(ent, inst)
    end    
end
--[[
local function makeactive(inst)
    inst.AnimState:PlayAnimation("off", true)
    inst.components.activatable.inactive = false
end

local function makeused(inst)
    inst.AnimState:PlayAnimation("flow_loop", true)
end
]]
local function reset(inst)
    print("RESET THE PUGALISK FOUNTAIN")
    inst.dry = nil
    inst.components.activatable.inactive = true
    inst.AnimState:PlayAnimation("flow_pre")
    inst.AnimState:PushAnimation("flow_loop", true) 
    inst.SoundEmitter:KillSound("burble")
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/fountain_LP", "burble")
    if inst.resettask then
        inst.resettask:Cancel()
        inst.resettask = nil
    end
    if inst.resettaskinfo then
        inst.resettaskinfo = nil
    end
end

local function deactivate(inst)    
    if not inst.resettask then
        inst.resettask, inst.resettaskinfo = inst:ResumeTask(TUNING.TOTAL_DAY_TIME, reset)
    end
end

local function OverrideActivateVerb(inst, doer)
	return subfmt(STRINGS.ACTIONS.ACTIVATE.FOUNTAIN, {name = inst.name})
end

local function onsave(inst,data)
    if inst.dry then
        data.dry = true
    end
    if inst.resettaskinfo then
        data.resettask = inst:TimeRemainingInTask(inst.resettaskinfo)
    end
end

local function onload(inst, data)
    if data then
        if data.resettask then
            if inst.resettask then inst.resettask:Cancel() inst.resettask = nil end
            inst.resettaskinfo = nil
            inst.resettask, inst.resettaskinfo = inst:ResumeTask(data.resettask, reset)
        end  
        if data.dry then        
            inst.AnimState:PlayAnimation("off", true)
            inst.SoundEmitter:KillSound("burble")
            inst.dry = true
            inst.components.activatable.inactive = false
        end         
    end
end

function OnLongUpdate(inst, dt)
    if inst.resettask then
        local new_time = math.max(inst:TimeRemainingInTask(inst.resettaskinfo) - dt, 0)

        inst.resettask:Cancel()
        inst.resettask = nil 
        inst.resettaskinfo = nil

        inst.resettask, inst.resettaskinfo = inst:ResumeTask(new_time, reset)
    end
end

function OnInit(inst)
    local drop = nil
	local plant = nil
	for k,v in pairs(Ents) do                
		if v:HasTag("lifeplant") then
			plant = true                                    
		end
		if v:HasTag("waterdrop") then
			drop = true
		end
		if plant and drop then
			break
		end
	end
	if not plant and not drop and inst.dry then               
		deactivate(inst)
	end
end

local function fn(Sim)
    local inst = CreateEntity()
    
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddMiniMapEntity()
	inst.entity:AddNetwork()
    
	inst.MiniMapEntity:SetIcon("pig_ruins_well.tex")

    MakeObstaclePhysics(inst, 2)
	
	inst:AddTag("pugalisk_fountain")
    inst:AddTag("pugalisk_avoids")
	
	inst.AnimState:SetBuild("python_fountain")    
    inst.AnimState:SetBank("fountain")
    inst.AnimState:PlayAnimation("flow_loop", true)

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
	
	inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/fountain_LP", "burble")
	
    inst:AddComponent("activatable")
    inst.components.activatable.OnActivate = OnActivate
    inst.components.activatable.inactive = true
   
	inst:AddComponent("inspectable")
    inst.components.inspectable:RecordViews()

    inst.deactivate = deactivate

    inst.OnSave = onsave 
    inst.OnLoad = onload

    inst.OnLongUpdate = OnLongUpdate
	inst.OverrideActivateVerb = OverrideActivateVerb

    inst:DoTaskInTime(0, OnInit)

    return inst
end

return Prefab("pugalisk_fountain", fn, assets, prefabs)