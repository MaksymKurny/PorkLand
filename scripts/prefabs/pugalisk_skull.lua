local assets=
{
	Asset("ANIM", "anim/snake_skull.zip"),
}

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	MakeInventoryFloatable(inst)
	
	inst:AddTag("pugalisk_skull")
    
    inst.AnimState:SetBank("snake_skull")
    inst.AnimState:SetBuild("snake_skull")
    inst.AnimState:PlayAnimation("idle")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
    
    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
	
	MakeHauntableLaunch(inst)

    return inst
end

return Prefab("pugalisk_skull", fn, assets) 