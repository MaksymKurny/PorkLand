local POCKETDIMENSIONCONTAINER_DEFS = {
	{
		name = "shadow",
		prefab = "shadow_container",
		ui = "anim/ui_portal_shadow_3x4.zip",
		widgetname = "shadow_container",
		tags = { "spoiler" },
	},
	{
	name = "root",
	prefab = "roottrunk",
	ui = "anim/ui_chester_shadow_3x4.zip",
	widgetname = "roottrunk",
	tags = { "spoiler" },
	},
}

return { POCKETDIMENSIONCONTAINER_DEFS = POCKETDIMENSIONCONTAINER_DEFS, }