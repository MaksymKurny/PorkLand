local assets=
{
	Asset("ANIM", "anim/hand_lens.zip"),
	Asset("ANIM", "anim/swap_hand_lens.zip"),
}

local function onequip(inst, owner) 
	owner.AnimState:OverrideSymbol("swap_object", "swap_hand_lens", "swap_hand_lens")
	owner.AnimState:Show("ARM_carry") 
	owner.AnimState:Hide("ARM_normal") 
	inst.components.finiteuses:SetConsumption(ACTIONS.PAN, owner:HasTag("treasure_hunter") and 1 or 2)
end

local function onunequip(inst, owner)
	owner.AnimState:Hide("ARM_carry") 
	owner.AnimState:Show("ARM_normal") 
end

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	
	MakeInventoryPhysics(inst)
	
	inst:AddTag("magnifying_glass")
	inst:AddTag("smeltable") -- Smelter

	inst.AnimState:SetBank("hand_lens")
	inst.AnimState:SetBuild("hand_lens")
	inst.AnimState:PlayAnimation("idle")
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	-------
	inst:AddComponent("finiteuses")
	inst.components.finiteuses:SetMaxUses(TUNING.MAGNIFYING_GLASS_USES * 2)
	inst.components.finiteuses:SetUses(TUNING.MAGNIFYING_GLASS_USES * 2)
	inst.components.finiteuses:SetConsumption(ACTIONS.SPY, 1)
	inst.components.finiteuses:SetOnFinished(inst.Remove)
	-------
	inst:AddComponent("weapon")
	inst.components.weapon:SetDamage(TUNING.MAGNIFYING_GLASS_DAMAGE)

	inst:AddComponent("tool")
    inst.components.tool:SetAction(ACTIONS.SPY)
	-------
	inst:AddComponent("inspectable")
	inst:AddComponent("inventoryitem")
	
	inst:AddComponent("equippable")
	inst.components.equippable:SetOnEquip(onequip)
	inst.components.equippable:SetOnUnequip(onunequip)
	
	inst:AddComponent("lighter")

	--inst.components.inventoryitem.imagename = "hand_lens"
	MakeHauntableLaunch(inst)

	return inst
end

--local function warbucks_mag_fn()
--	local inst = fn()
--	inst:RemoveComponent("finiteuses")
--end

return Prefab("magnifying_glass", fn, assets)