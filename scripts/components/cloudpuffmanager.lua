require("constants")

local easing = require("easing")

local function spawnCloudPuff(x, y, z)
	local cloudpuff = SpawnPrefab( "cloudpuff" )
	cloudpuff.Transform:SetPosition(x, y, z)
end

local function isSky(map, x, y, z)
	return (WORLD_TILES.IMPASSABLE == map:GetTileAtPoint(x, y, z)) and
           (WORLD_TILES.IMPASSABLE == map:GetTileAtPoint(x + 1.0, y, z)) and
           (WORLD_TILES.IMPASSABLE == map:GetTileAtPoint(x - 1.0, y, z)) and
           (WORLD_TILES.IMPASSABLE == map:GetTileAtPoint(x, y, z + 1.0)) and
           (WORLD_TILES.IMPASSABLE == map:GetTileAtPoint(x, y, z - 1.0))
end

local CloudPuffManager = Class(function(self, inst)
	self.inst = inst

	self.cloudpuff_per_sec = 1.5
	self.cloudpuff_spawn_rate = 0

	self.inst:StartUpdatingComponent(self)
end)

local function getCloudPuffRadius()
	-- From values from camera_volcano.lua, camera range 30 to 100
	local percent = (TheCamera:GetDistance() - 30) / (70)
	local row_radius = (24 - 16) * percent + 16
	local col_radius = (8 - 2) * percent + 2

	return row_radius, col_radius
end

function CloudPuffManager:OnUpdate(dt)
	local map = TheWorld.Map
	if map == nil then return end

	for _, player in ipairs(AllPlayers) do
		local px, py, pz = player.Transform:GetWorldPosition()

		self.cloudpuff_spawn_rate = self.cloudpuff_spawn_rate + self.cloudpuff_per_sec * dt

		local radius = getCloudPuffRadius()

		while self.cloudpuff_spawn_rate > 10.0 do
			local dx, dz = radius * UnitRand(), radius * UnitRand()
			local x, y, z = px + dx, py, pz + dz

			if isSky(map, x, y, z) then
				spawnCloudPuff(x, y, z)
			end

			self.cloudpuff_spawn_rate = self.cloudpuff_spawn_rate - 1.0
		end
	end
end

return CloudPuffManager
