local AddAction = AddAction
local AddComponentAction = AddComponentAction
GLOBAL.setfenv(1, GLOBAL)

local PL_ACTIONS = {
    PEAGAWK_TRANSFORM = Action({}),
    DIGDUNG = Action({mount_valid=true}),
    MOUNTDUNG = Action({}),
    DISLODGE = Action({}),
    SPECIAL_ACTION = Action({distance = 1.2}),
    SPECIAL_ACTION2 = Action({distance = 1.2}),
    BARK = Action({distance = 3}),
    RANSACK = Action({distance = 0.5}),
	INFEST = Action({distance = 0.5}),
	STOCK = Action({}),
	FIX = Action({}),
	CUREPOISON = Action({}),
	PAN = Action({}),
	GAS = Action({mount_valid=true, distance = 1.5}),
	SPY = Action({mount_valid=true, distance = 2.0}),
}

for name, ACTION in pairs(PL_ACTIONS) do
    ACTION.id = name
    ACTION.str = STRINGS.ACTIONS[name] or "PL_ACTION"
    AddAction(ACTION)
end
-----------------------------------------------------------------------------------------
ACTIONS.PEAGAWK_TRANSFORM.fn = function(act)
    return true -- Dummy action for flup hiding
end

-----------------------------------------------------------------------------------------
ACTIONS.DIGDUNG.fn = function(act)
	act.target.components.workable:WorkedBy_Internal(act.doer, 1)
	return true
end

ACTIONS.MOUNTDUNG.fn = function(act)
	act.doer.dung_target:Remove()
	act.doer:AddTag("hasdung") 
	act.doer.dung_target = nil
	return true
end
-----------------------------------------------------------------------------------------
ACTIONS.CUREPOISON.strfn = function(act)
    if act.invobject and act.invobject:HasTag("venomgland") then
        return "GLAND"
    end
end

ACTIONS.CUREPOISON.fn = function(act)
    if act.invobject and act.invobject.components.poisonhealer then
        local target = act.target or act.doer
        return act.invobject.components.poisonhealer:Cure(target)
    end
end

AddComponentAction("USEITEM", "poisonhealer", function(inst, doer, target, actions, right)
    if inst:HasTag("poison_antidote") and target and target:HasTag("poisonable") then
        if target:HasTag("poison") or
        (target:HasTag("player") and
        ((target.components.poisonable and target.components.poisonable:IsPoisoned()) or
        (target.player_classified and target.player_classified.ispoisoned:value()) or
        inst:HasTag("poison_vaccine"))) then
            table.insert(actions, ACTIONS.CUREPOISON)
        end
    end
end)

AddComponentAction("INVENTORY", "poisonhealer", function(inst, doer, actions, right)
    if inst:HasTag("poison_antidote") and doer:HasTag("poisonable") and
    (doer:HasTag("player") and
    ((doer.components.poisonable and doer.components.poisonable:IsPoisoned()) or
    (doer.player_classified and doer.player_classified.ispoisoned:value()) or
    inst:HasTag("poison_vaccine"))) then
        table.insert(actions, ACTIONS.CUREPOISON)
    end
end)
-----------------------------------------------------------------------------------------
ACTIONS.DISLODGE.fn = function(act)
	if act.target.components.dislodgeable then
		act.target.components.dislodgeable:Dislodge(act.doer)
		-- action with inventory object already explicitly calls OnUsedAsItem
		if not act.invobject and act.doer and act.doer.components.inventory and act.doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
			local invobject = act.doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
			if invobject.components.finiteuses then
				invobject.components.finiteuses:OnUsedAsItem(ACTIONS.DISLODGE)
			end
		end
		return true
	end
end

AddComponentAction("EQUIPPED", "dislodger", function(inst, doer, target, actions, right)
    if target:HasTag("dislodgeable") then
        if not right then
            table.insert(actions, ACTIONS.DISLODGE)
        end
    end
end)

ACTIONS.DISLODGE.validfn = function(act)
    return (act.target.components.dislodgeable and act.target.components.dislodgeable:CanBeDislodged()) or
        (act.target.components.workable and act.target.components.workable:CanBeWorked() and 
			act.target.components.workable:GetWorkAction() == ACTIONS.DISLODGE)
end

-----------------------------------------------------------------------------------------
ACTIONS.INFEST.fn = function(act)
	if not act.doer.infesting then
		act.doer.components.infester:Infest(act.target)
	end
	return true
end

ACTIONS.SPECIAL_ACTION.fn = function(act)
	if act.doer.special_action then
		act.doer.special_action(act)
		return true
	end
end

ACTIONS.SPECIAL_ACTION2.fn = function(act)
	if act.doer.special_action2 then
		act.doer.special_action2(act)
		return true
	end
end
-----------------------------------------------------------------------------------------

ACTIONS.PAN.fn = function(act)
	if act.target.components.workable and act.target.components.workable.action == ACTIONS.PAN then
		local numworks = 1

		if act.invobject and act.invobject.components.tool then
			numworks = act.invobject.components.tool:GetEffectiveness(ACTIONS.PAN)
		elseif act.doer and act.doer.components.worker then
			numworks = act.doer.components.worker:GetEffectiveness(ACTIONS.PAN)
		end
		act.target.components.workable:WorkedBy_Internal(act.doer, numworks)

		if act.target.components.citypossession and act.target.components.citypossession.enabled then
			local world = TheWorld
			if world.components.cityalarms then
				world.components.cityalarms:ChangeStatus(act.target.components.citypossession.cityID, true, act.doer)			
			end				
		end			
	end
	return true
end

ACTIONS.PAN.validfn = function(act)
    return (act.target.components.workable and act.target.components.workable:CanBeWorked() and 
		act.target.components.workable:GetWorkAction() == ACTIONS.PAN)
end

-----------------------------------------------------------------------------------------

ACTIONS.GAS.fn = function(act)
	if act.invobject and act.invobject.components.gasser then
	    local pos = act:GetActionPoint()
		if pos == nil then
			pos = act.target:GetPosition()
		end
		act.invobject.components.gasser:Gas(pos)
		return true
	end
end

AddComponentAction("EQUIPPED", "gasser", function(inst, doer, target, actions, right)
	if right then
		table.insert(actions, ACTIONS.GAS)
	end
end)

-----------------------------------------------------------------------------------------

ACTIONS.SPY.fn = function(act)
	if act.target and act.target.components.mystery then
		act.target.components.mystery:Investigate(act.doer)
		return true
	elseif act.target and act.target.components.mystery_door then
		act.target.components.mystery_door:Investigate(act.doer)
		return true
	end
end

ACTIONS.SPY.validfn = function(act)
    return act.target:HasTag("mystery") or (act.target.components.workable and act.target.components.workable:CanBeWorked() and 
		act.target.components.workable:GetWorkAction() == ACTIONS.SPY)
end
-----------------------------------------------------------------------------------------
ACTIONS.FIX.fn = function(act)
	if act.target then
		local target = act.target
		local numworks = 1
		target.components.workable:WorkedBy_Internal(act.doer, numworks)
	--	return target:fix(act.doer)		
	end
end
ACTIONS.STOCK.fn = function(act)
	if act.target then		
		act.target.restock(act.target, true)
		act.doer.changestock = nil
		return true
	end
end
-----------------------------------------------------------------------------------------
local _HAMMERvalidfn = ACTIONS.HAMMER.validfn
function ACTIONS.HAMMER.validfn(act)
    if act.invobject:HasTag("fixable_crusher") and not act.target:HasTag("fixable") then
		return false
	end
	return _HAMMERvalidfn(act)
end

-----------------------------------------------------------------------------------------
local _GIVEfn = ACTIONS.GIVE.fn
function ACTIONS.GIVE.fn(act)
	if act.invobject.components.inventoryitem then
		if act.target.components.shelfer then
			act.target.components.shelfer:AcceptGift(act.doer, act.invobject)
			return true
		end
	end
	return _GIVEfn(act)
end
AddComponentAction("USEITEM", "inventoryitem", function(inst, doer, target, actions, right)
	if target:HasTag("shelfer") then        
		table.insert(actions, ACTIONS.GIVE)        
	end  
end)
-----------------------------------------------------------------------------------------
-- Shelf and Shop --
local _PICKUPfn = ACTIONS.PICKUP.fn
function ACTIONS.PICKUP.fn(act)
	if act.target and act.target.components.inventoryitem and act.target.components.shelfer then
		local item  = act.target.components.shelfer:GetGift()
		if item then
			item:AddTag("cost_one_oinc")
			if act.target.components.shelfer.shelf and not act.target.components.shelfer.shelf:HasTag("playercrafted") then
				if act.doer.components.shopper and act.doer.components.shopper:IsWatching(item) then 
					if act.doer.components.shopper:CanPayFor(item) then 
						act.doer.components.shopper:PayFor(item)
					else 			
						return false, "CANTPAY"
					end
				else
					if act.target.components.shelfer.shelf and act.target.components.shelfer.shelf.curse then
						act.target.components.shelfer.shelf.curse(act.target)
					end						
				end
			end
			item:RemoveTag("cost_one_oinc")
			if item.components.perishable then 
				item.components.perishable:StartPerishing() 
			end		
			act.target = act.target.components.shelfer:GiveGift()
		end
	end
	return _PICKUPfn(act)
end
-----------------------------------------------------------------------------------------
function ACTIONS.PICK.strfn(act)
	if act.target:HasTag("pick_digin") then
		return "DIGIN"
	elseif act.target:HasTag("flippable") then
		return "FLIP"
	end
end

local _PICKUPstrfn = ACTIONS.PICKUP.strfn
function ACTIONS.PICKUP.strfn(act)
	return act.target ~= nil and
		(act.target:HasTag("shelfer") and "SHELF")
		or _PICKUPstrfn(act)
end

local _GIVEstrfn = ACTIONS.GIVE.strfn
function ACTIONS.GIVE.strfn(act)
	return act.target ~= nil and
		(act.target:HasTag("shelfer") and "SHELF")
		or _GIVEstrfn(act)
end

function ACTIONS.COOK.strfn(act)
	if act.target and act.target.components.melter then
		return "SMELT"
	end
end 
-----------------------------------------------------------------------------------------
-- Patch for smelter things --
local _COOKfn = ACTIONS.COOK.fn
function ACTIONS.COOK.fn(act)
    if act.target.components.melter then
		if act.target.components.melter:IsCooking() then
            --Already cooking
            return true
        end
        local container = act.target.components.container
        if container ~= nil and container:IsOpenedByOthers(act.doer) then
            return false, "INUSE"
        elseif not act.target.components.melter:CanCook() then
            return false
        end
        act.target.components.melter:StartCooking(act.doer)
        return true
	end
	
	return _COOKfn(act)
end

local _HARVESTvalidfn = ACTIONS.HARVEST.validfn
function ACTIONS.HARVEST.validfn(act, ...)
    if act.target and act.target.components.melter then --Dont continue to harvest if it cannot be harvested, fixes a crash trying to spawn a nil -Half
		return act.target:HasTag("donecooking")
    else
        return (_HARVESTvalidfn and _HARVESTvalidfn(act, ...)) or true --if a validfn is added use that or send back true so everything works normally
    end
end

local _HARVESTfn = ACTIONS.HARVEST.fn
function ACTIONS.HARVEST.fn(act)
     if act.target.components.melter then
        return act.target.components.melter:Harvest(act.doer)	
	else
		return _HARVESTfn(act)
	end
end

AddComponentAction("SCENE", "melter", function(inst, doer, actions, right)
    if inst:HasTag("donecooking") and doer.replica.inventory then
        table.insert(actions, ACTIONS.HARVEST)
    end
end)
-----------------------------------------------------------------------------------------

-- SCENE        using an object in the world
-- USEITEM      using an inventory item on an object in the world
-- POINT        using an inventory item on a point in the world
-- EQUIPPED     using an equiped item on yourself or a target object in the world
-- INVENTORY    using an inventory item
local PL_COMPONENT_ACTIONS =
{
    SCENE = { -- args: inst, doer, actions, right

    },

    USEITEM = { -- args: inst, doer, target, actions, right
    },

    POINT = { -- args: inst, doer, pos, actions, right, target

    },

    EQUIPPED = { -- args: inst, doer, target, actions, right

    },

    INVENTORY = { -- args: inst, doer, actions, right


    },
    ISVALID = { -- args: inst, action, right
        dislodgeable = function(inst, action, right)
            return action == ACTIONS.DISLODGE and inst:HasTag("DISLODGE_workable")
		end,
		mystery = function(inst, action, right)
            return action == ACTIONS.SPY and inst:HasTag("mystery")
		end,
    },
}

for actiontype, actons in pairs(PL_COMPONENT_ACTIONS) do
    for component, fn in pairs(actons) do
        AddComponentAction(actiontype, component, fn)
    end
end




-- hack
local COMPONENT_ACTIONS = ToolUtil.GetUpvalue(EntityScript.CollectActions, "COMPONENT_ACTIONS")
local SCENE = COMPONENT_ACTIONS.SCENE
local USEITEM = COMPONENT_ACTIONS.USEITEM
local POINT = COMPONENT_ACTIONS.POINT
local EQUIPPED = COMPONENT_ACTIONS.EQUIPPED
local INVENTORY = COMPONENT_ACTIONS.INVENTORY

_wateryprotection = COMPONENT_ACTIONS.EQUIPPED.wateryprotection
function EQUIPPED.wateryprotection(inst, doer, target, actions, right, ...)
    if right and target:HasTag("waterneeded") then
        table.insert(actions, ACTIONS.POUR_WATER)
    else
        _wateryprotection(inst, doer, target, actions, right, ...)
    end
end