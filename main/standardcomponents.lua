GLOBAL.setfenv(1, GLOBAL)

function RemoveBlowInHurricane() end

function MakeNoPhysics(inst, mass, rad)
    inst.entity:AddPhysics()
    inst.Physics:SetMass(mass)
    inst.Physics:SetCapsule(rad, 1)
    inst.Physics:SetFriction(0)
    inst.Physics:SetDamping(5)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
end

function MakeAmphibiousCharacterPhysics(inst, mass, rad)
    local phys = inst.entity:AddPhysics()
    phys:SetMass(mass)
    phys:SetCapsule(rad, 1)
    phys:SetFriction(0)
    phys:SetDamping(5)
    phys:SetCollisionGroup(COLLISION.CHARACTERS)
    phys:ClearCollisionMask()
    phys:CollidesWith((TheWorld.has_ocean and COLLISION.GROUND) or COLLISION.WORLD)
    phys:CollidesWith(COLLISION.OBSTACLES)
    phys:CollidesWith(COLLISION.CHARACTERS)
    return phys
end

--Physics
local function BuildMesh(vertices, height)
    local triangles = {}
    local y0 = 0
    local y1 = height
 
    local idx0 = #vertices
    for idx1 = 1, #vertices do
        local x0, z0 = vertices[idx0].x, vertices[idx0].z
        local x1, z1 = vertices[idx1].x, vertices[idx1].z
 
        table.insert(triangles, x0)
        table.insert(triangles, y0)
        table.insert(triangles, z0)
 
        table.insert(triangles, x0)
        table.insert(triangles, y1)
        table.insert(triangles, z0)
 
        table.insert(triangles, x1)
        table.insert(triangles, y0)
        table.insert(triangles, z1)
 
        table.insert(triangles, x1)
        table.insert(triangles, y0)
        table.insert(triangles, z1)
 
        table.insert(triangles, x0)
        table.insert(triangles, y1)
        table.insert(triangles, z0)
 
        table.insert(triangles, x1)
        table.insert(triangles, y1)
        table.insert(triangles, z1)
 
        idx0 = idx1
    end
    return triangles
end

Physics.SetRectangle = function(self, depth, height, width)-- Ported from "engine" :D
	local vertexes = {
		Vector3(width, 0, -depth),
		Vector3(-width, 0, -depth),
		Vector3(-width, 0, depth),
		Vector3(width, 0, depth),
	}
	self:SetTriangleMesh(BuildMesh(vertexes, height))
end

function MakeInteriorPhysics(inst, depth, height, width)
    height = height or 20

    inst:AddTag("blocker")
    inst.entity:AddPhysics()
    inst.Physics:SetMass(0) 
    --inst.Physics:SetCollisionGroup(COLLISION.INTWALL) -- GetWorldCollision()
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)    
    inst.Physics:SetRectangle(depth, height, width)
end