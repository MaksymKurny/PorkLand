local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local BirdSpawner = require("components/birdspawner")

local birdvstile = {	
	[WORLD_TILES.RAINFOREST]	= {"kingfisher","parrot_blue"},
	[WORLD_TILES.DEEPRAINFOREST]	= {"parrot_blue","kingfisher"},
	[WORLD_TILES.GASJUNGLE]	= {"parrot_blue"},			
	[WORLD_TILES.FOUNDATION] = {"pigeon","pigeon_swarm","pigeon_swarm","crow"},
	[WORLD_TILES.FIELDS] = {"robin","crow"},
	[WORLD_TILES.SUBURB] = {"robin","crow","pigeon"},
	[WORLD_TILES.PLAINS] = {"robin","crow","kingfisher"},
	[WORLD_TILES.PAINTED] =  {"kingfisher","crow"},
	--[WORLD_TILES.BATTLEGROUND] = {},
	--[WORLD_TILES.INTERIOR] = {},
	--[WORLD_TILES.LILYPOND] = {},
}

local SCARYTOPREY_TAGS = { "scarytoprey" }
local function IsDangerNearby(x, y, z)
    local ents = TheSim:FindEntities(x, y, z, 8, SCARYTOPREY_TAGS)
    return next(ents) ~= nil
end

local function RelevantSpawnBird(self, bird_prefab, spawnpoint, ignorebait)
    local bird = SpawnPrefab(bird_prefab)
    if math.random() < .5 then
        bird.Transform:SetRotation(180)
    end
    if bird:HasTag("bird") then
        spawnpoint.y = 15
    end

    if bird.components.eater and not ignorebait then
        local bait = TheSim:FindEntities(spawnpoint.x, 0, spawnpoint.z, 15)
        for k, v in pairs(bait) do
            local x, y, z = v.Transform:GetWorldPosition()

            if IsOnFlood(x, y, z) then -- birds can't spawn at flood
                break
            end

            if bird.components.eater:CanEat(v) and not v:IsInLimbo() and v.components.bait and
                not (v.components.inventoryitem and v.components.inventoryitem:IsHeld()) and not IsDangerNearby(x, y, z) then
                spawnpoint.x, spawnpoint.z = x, z
                bird.bufferedaction = BufferedAction(bird, v, ACTIONS.EAT)
                break
            elseif v.components.trap and v.components.trap.isset and
                (not v.components.trap.targettag or bird:HasTag(v.components.trap.targettag)) and
                not v.components.trap.issprung and math.random() < TUNING.BIRD_TRAP_CHANCE and
                not IsDangerNearby(x, y, z) and (bird.components.floater ~= nil or not IsOnWater(x, y, z)) then -- when waters are merged change this to (bird.components.floater ~= nil or _map:IsPassableAtPoint(x, y, z))
                spawnpoint.x, spawnpoint.z = x, z
                break
            end
        end
    end

    bird.Physics:Teleport(spawnpoint:Get())

    return bird
end

IAENV.AddComponentPostInit("birdspawner", function(cmp)
    local _SpawnBird = cmp.SpawnBird
    function cmp:SpawnBird(spawnpoint, ignorebait)
        if IsOnFlood(spawnpoint:Get()) then -- birds can't spawn at flood
            return
        end

        local tile = TheWorld.Map:GetTileAtPoint(spawnpoint:Get())
        local bird_prefab = nil

        local climate = GetClimate(spawnpoint)
        if IsClimate(climate, "porkland") then
			if birdvstile[tile] ~= nil and not TheWorld.state.iswinter then
				if type(birdvstile[tile]) == "table" then
					bird_prefab = GetRandomItem(birdvstile[tile])
				else
					bird_prefab = birdvstile[tile]
				end
			else
				return -- SW explicitly does not spawn birds on undefined turfs
			end
        else
            return _SpawnBird(self, spawnpoint, ignorebait)
        end

        return RelevantSpawnBird(self, bird_prefab, spawnpoint, ignorebait)
    end

    function cmp:PickBird(spawnpoint)
        local world = TheWorld
        local map = world.Map
        local tile = map:GetTileAtPoint(spawnpoint.x, spawnpoint.y, spawnpoint.z)
        if world.state.iswinter then
            -- if tile == WORLD_TILES.BEACH and self.seagulspawn then
			return nil
        end
    end
end)
