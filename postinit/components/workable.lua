local PLENV = env
GLOBAL.setfenv(1, GLOBAL)

local Workable = require("components/workable")

local _WorkedBy_Internal = Workable.WorkedBy_Internal
function Workable:WorkedBy_Internal(worker, numworks)
	local was_fixable = self.inst.components.fixable ~= nil	
	if self.workleft <= 1 then
		if worker and worker.components.inventory and worker.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
            local tool = worker.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) 
            if tool and tool:HasTag("fixable_crusher") and self.inst.components.fixable then
                self.inst:RemoveComponent("fixable")
            end
        end

        if self.inst.components.door and was_fixable and not self.inst.components.fixable then
            GetInteriorSpawner():DestroyInteriorByDoor(self.inst)
        end
	end
	_WorkedBy_Internal(self, worker, numworks)
end

--[[function Workable:WorkedBy_Internal(worker, numworks)
	print("WorkedBy", worker, self.workleft)
	numworks = numworks or 1
	if self.workmultiplierfn ~= nil then
		numworks = numworks * (self.workmultiplierfn(self.inst, worker, numworks) or 1)
	end
	if numworks > 0 then
		if self.workleft <= 1 then -- if there is less that one full work remaining, then just finish it. This is to handle the case where objects are set to only one work and not planned to handled something like 0.5 numworks
			self.workleft = 0
		else
			self.workleft = self.workleft - numworks
			if self.workleft < 0.01 then -- NOTES(JBK): Floating points are possible with work efficiency modifiers so cut out the epsilon.
				self.workleft = 0
			end
		end
	end
    self.lastworktime = GetTime()

    worker:PushEvent("working", { target = self.inst })
    self.inst:PushEvent("worked", { worker = worker, workleft = self.workleft })

    if self.onwork ~= nil then
        self.onwork(self.inst, worker, self.workleft, numworks)
    end

	local was_fixable = self.inst.components.fixable ~= nil
    if self.workleft <= 0 then
		if worker and worker.components.inventory and worker.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
            local tool = worker.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) 
            if tool and tool:HasTag("fixable_crusher") and self.inst.components.fixable then
                self.inst:RemoveComponent("fixable")
            end
        end
	
        if self.inst.components.door and was_fixable and not self.inst.components.fixable then
            GetInteriorSpawner():DestroyInteriorByDoor(self.inst)
        end
	
        local isplant =
            self.inst:HasTag("plant") and
            not self.inst:HasTag("burnt") and
            not (self.inst.components.diseaseable ~= nil and self.inst.components.diseaseable:IsDiseased())
        local pos = isplant and self.inst:GetPosition() or nil

        if self.onfinish ~= nil then
            self.onfinish(self.inst, worker)
        end
        self.inst:PushEvent("workfinished", { worker = worker })
        worker:PushEvent("finishedwork", { target = self.inst, action = self.action })
        if isplant then
            TheWorld:PushEvent("plantkilled", { doer = worker, pos = pos, workaction = self.action }) --this event is pushed in other places too
        end
    end
end]]