GLOBAL.setfenv(1, GLOBAL)

local Hatchable = require("components/hatchable")

function Hatchable:SetUpdateFn(fn)
    self.onupdatefn = fn
end

local _OnUpdate = Hatchable.OnUpdate
function Hatchable:OnUpdate(dt)
    if self.inst:HasTag("ro_bin_egg") then -- Protection from death
		self.hatchfailtime = self.discomfort + dt + 1
	end
	
	_OnUpdate(self, dt)

    if self.onupdatefn then
        self.onupdatefn(self.inst, dt)
    end
end
