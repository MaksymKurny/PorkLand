local AddPlayerPostInit = AddPlayerPostInit
GLOBAL.setfenv(1, GLOBAL)

local function IsPoisonDisabled()
	return TheWorld and TheWorld.components.globalsettings and 
		TheWorld.components.globalsettings.settings.poisondisabled and 
			TheWorld.components.globalsettings.settings.poisondisabled == true
end

local function BeginGas(inst)
	local safe = false
	-- check armour
	if inst.components.inventory then
		for k,v in pairs (inst.components.inventory.equipslots) do
			if v.components.equippable and v.components.equippable:IsPoisonGasBlocker() then
				safe = true
			end		
		end
	end

	if inst:HasTag("has_gasmask") or IsPoisonDisabled() then
		safe = true
	end
	
	if not safe then
		inst.components.health:DoGasDamage(TUNING.GAS_DAMAGE_PER_INTERVAL)			
		inst:PushEvent("poisondamage")	
		inst.components.talker:Say(GetString(inst.prefab, "ANNOUNCE_GAS_DAMAGE"))
	end
end

local function OnChangeArea(inst, area, force)
	-- DST lunacy
	local enable_lunacy = area ~= nil and area.tags and table.contains(area.tags, "lunacyarea")
	inst.components.sanity:EnableLunacy(enable_lunacy, "lunacyarea")
	
	-- PL Gas
	if force == true or (area and area.tags and table.contains(area.tags, "Gas_Jungle")) then
		if inst.gasTask == nil then
			inst.gasTask = inst:DoPeriodicTask(TUNING.GAS_INTERVAL, BeginGas)
		end
	elseif inst.gasTask then 
		inst.gasTask:Cancel()
		inst.gasTask = nil
	end
end

local function OnDeath(self, data)
	if self.components.poisonable then
		self.components.poisonable:SetBlockAll(true)
	end
end

local function OnRespawnFromGhost(self, data)
	if self.components.poisonable and not self:HasTag("beaver") then
		self.components.poisonable:SetBlockAll(false)
	end
end

AddPlayerPostInit(function(inst)
    if not TheWorld.ismastersim then
        return
    end
	
	inst:AddComponent("infestable")
	inst:AddComponent("shopper")
	--inst:AddComponent("canopytracker") -- lot of problem
	
	inst:ListenForEvent("changearea", OnChangeArea)
	inst.OnGasChange = OnChangeArea
    inst:ListenForEvent("death", OnDeath)
    inst:ListenForEvent("respawnfromghost", OnRespawnFromGhost)

end)
