local AddStategraphState = AddStategraphState
local AddStategraphPostInit = AddStategraphPostInit
local AddStategraphActionHandler = AddStategraphActionHandler
GLOBAL.setfenv(1, GLOBAL)

local TIMEOUT = 2

local actionhandlers = {
	ActionHandler(ACTIONS.LIGHT, function(inst) 
		local equipped = inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)

		if equipped and equipped:HasTag("magnifying_glass") then
			return "investigate_start"
		else
			return "catchonfire"
		end
	end),

    ActionHandler(ACTIONS.HACK, function(inst)
        if inst:HasTag("beaver") then
            return not inst.sg:HasStateTag("gnawing") and "gnaw" or nil
        end
        return not inst.sg:HasStateTag("prehack") and "hack_start" or nil
    end),
    ActionHandler(ACTIONS.SHEAR, function(inst)
        return not inst.sg:HasStateTag("preshear") and "shear_start" or nil
    end),
	ActionHandler(ACTIONS.SPY, function(inst, action)
		if not inst.sg:HasStateTag("preinvestigate") then
			return action.invobject:HasTag("goggles") and "goggle" or "investigate"
		end
	end),
    ActionHandler(ACTIONS.DISLODGE, "tap"),
	
	--ActionHandler(ACTIONS.REPAIRBOAT, "dolongaction"),
	
    ActionHandler(ACTIONS.CUREPOISON, function(inst, action)
        return (not action.target or action.target == inst) and "curepoison" or "give"
    end),
	
	ActionHandler(ACTIONS.PAN, function(inst) 
		return not inst.sg:HasStateTag("panning") and "pan_start" or nil
	end),
	
	ActionHandler(ACTIONS.GAS, function(inst)
		return "crop_dust"
	end),
}

local states = {
    State{ name = "pan_start",
        tags = {"prepan", "panning", "working"},
        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("pan_pre")
        end,
        
        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end ),
            EventHandler("animover", function(inst) inst.sg:GoToState("pan") end),
        },
    },
    
    State{
        name = "pan",
        tags = {"prepan", "panning", "working"},
        onenter = function(inst)
            inst.sg.statemem.action = inst:GetBufferedAction()
            inst.AnimState:PlayAnimation("pan_loop",true) 
            inst.sg:SetTimeout(1 + math.random())            
        end,
    
        timeline=
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 

            TimeEvent((6+15)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 
            TimeEvent((14+15)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 

            TimeEvent((6+30)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 
            TimeEvent((14+30)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 

            TimeEvent((6+45)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 
            TimeEvent((14+45)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end),             

            TimeEvent((6+60)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end), 
            TimeEvent((14+60)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/harvested/pool/pan") end),                         
        },

  
        ontimeout = function(inst)
            inst:PerformBufferedAction() 
            inst.sg:GoToState("idle", "pan_pst")
        end,        

        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle","pan_pst") end ),
            --EventHandler("animover", function(inst) 
            --    inst.sg:GoToState("idle","pan_pst")
            --end ),            
        },        
    },
	
	State{
        name = "hack_start",
        tags = {"prehack", "hacking", "working"},

        onenter = function(inst)
            inst.components.locomotor:Stop()

            if not inst:HasTag("working") then
                local action = inst:GetBufferedAction()
                local tool = action ~= nil and action.invobject or nil
                local hacksymbols = tool ~= nil and tool.hack_overridesymbols or nil
                if hacksymbols ~= nil then
                    hacksymbols[3] = tool:GetSkinBuild()
                    if hacksymbols[3] ~= nil then
                        inst.AnimState:OverrideItemSkinSymbol("swap_machete", hacksymbols[3], hacksymbols[1], tool.GUID, hacksymbols[2])
                    else
                        inst.AnimState:OverrideSymbol("swap_machete", hacksymbols[1], hacksymbols[2])
                    end
                    inst.AnimState:PlayAnimation("hack_pre")
                    inst.AnimState:PushAnimation("hack_lag", false)
                else
                    inst.AnimState:PlayAnimation("chop_pre")
                    inst.AnimState:PushAnimation("chop_lag", false)
                end
            end

            inst:PerformPreviewBufferedAction()
            inst.sg:SetTimeout(TIMEOUT)
        end,

        onupdate = function(inst)
            if inst:HasTag("working") then
                if inst.entity:FlattenMovementPrediction() then
                    inst.sg:GoToState("idle", "noanim")
                end
            elseif inst.bufferedaction == nil then
                inst.sg:GoToState("idle")
            end
        end,

        ontimeout = function(inst)
            inst:ClearBufferedAction()
            inst.sg:GoToState("idle")
        end
    },

    State{
        name = "shear_start",
        tags = {"preshear", "working"},

        onenter = function(inst)
            inst.components.locomotor:Stop()

            if not inst:HasTag("working") then
                inst.AnimState:PlayAnimation("tamp_pre")
            end

            inst:PerformPreviewBufferedAction()
            inst.sg:SetTimeout(TIMEOUT)
        end,

        onupdate = function(inst)
            if inst:HasTag("working") then
                if inst.entity:FlattenMovementPrediction() then
                    inst.sg:GoToState("idle", "noanim")
                end
            elseif inst.bufferedaction == nil then
                inst.AnimState:PlayAnimation("pickaxe_pst")
                inst.sg:GoToState("idle")
            end
        end,

        ontimeout = function(inst)
            inst:ClearBufferedAction()
            inst.sg:GoToState("idle")
        end
    },

    State{
        name = "tap",
        tags = {"working", "busy"},

        onenter = function(inst)
            inst.components.locomotor:Stop()

            if not inst:HasTag("working") then
                inst.AnimState:PlayAnimation("tamp_pre")
            end

            inst:PerformPreviewBufferedAction()
            inst.sg:SetTimeout(TIMEOUT)
        end,

        onupdate = function(inst)
            if inst:HasTag("working") then
                if inst.entity:FlattenMovementPrediction() then
                    inst.sg:GoToState("idle", "noanim")
                end
            elseif inst.bufferedaction == nil then
                inst.AnimState:PlayAnimation("pickaxe_pst")
                inst.sg:GoToState("idle")
            end
        end,

        ontimeout = function(inst)
            inst:ClearBufferedAction()
            inst.sg:GoToState("idle")
        end
    },
	State{
        name = "speargun",
        tags = {"attack", "notalking", "abouttoattack"},
        
        onenter = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end
			inst.replica.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("speargun")
			
			local act = inst:GetBufferedAction()
			if act then 
				inst.sg.statemem.target = act.target
				inst.sg.statemem.target_position = Vector3(act.target.Transform:GetWorldPosition())
				
				inst:PerformPreviewBufferedAction()
				if act.target and act.target:IsValid() then
					inst:FacePoint(act.target:GetPosition())
				end
			end
        end,
        
        timeline=
        {
            TimeEvent(12*FRAMES, function(inst)
				if not inst.sg.statemem.chained and inst.sg.statemem.projectiledelay == nil then
                   inst:ClearBufferedAction()
                   inst.sg:RemoveStateTag("abouttoattack")
				end
				-- inst.replica.combat:DoAttack(inst.sg.statemem.target)
                if inst.replica.combat:GetWeapon() and inst.replica.combat:GetWeapon():HasTag("blunderbuss") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/items/weapon/blunderbuss_shoot")
                    local cloud = SpawnPrefab("cloudpuff")
                    local pt = Vector3(inst.Transform:GetWorldPosition())

					local angle
					if inst.replica.combat.target and inst.replica.combat.target:IsValid() then
                    	angle = (inst:GetAngleToPoint(inst.replica.combat.target.Transform:GetWorldPosition()) -90)*DEGREES
					else
						angle = (inst:GetAngleToPoint(inst.sg.statemem.target_position.x, inst.sg.statemem.target_position.y, inst.sg.statemem.target_position.z) -90)*DEGREES
					end	                	
					inst.sg.statemem.target_position = nil
					
                    local DIST = 1.5
                    local offset = Vector3(DIST * math.cos( angle+(PI/2) ), 0, -DIST * math.sin( angle+(PI/2) ))

                    local y = inst.replica.rider:IsRiding() and 4.5 or 2
                    cloud.Transform:SetPosition(pt.x + offset.x, y, pt.z + offset.z)
                else
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/use_speargun")
                end
            end),
            TimeEvent(20*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
        },
        
        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
		onexit = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
			if inst.sg:HasStateTag("abouttoattack") and inst.replica.combat then
                inst.replica.combat:CancelAttack()
            end
        end,
    },
	
	State{
        name = "curepoison",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("quick_eat_pre")
            inst.AnimState:PushAnimation("quick_eat_lag", false)

            inst:PerformPreviewBufferedAction()
            inst.sg:SetTimeout(TIMEOUT)
        end,

        onupdate = function(inst)
            if inst:HasTag("busy") then
                if inst.entity:FlattenMovementPrediction() then
                    inst.sg:GoToState("idle", "noanim")
                end
            elseif inst.bufferedaction == nil then
                inst.sg:GoToState("idle")
            end
        end,

        ontimeout = function(inst)
            inst:ClearBufferedAction()
            inst.sg:GoToState("idle")
        end,
    },
	
	State{ name = "crop_dust",
        tags = {"busy","canrotate"},
        
        onenter = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end

            local buffaction = inst:GetBufferedAction()
            if buffaction ~= nil then
                inst:PerformPreviewBufferedAction()

                if buffaction.target ~= nil and buffaction.target:IsValid() then
					inst:FacePoint(buffaction.target:GetPosition())
				end
			end
           
            --dumptable(action,1,1,1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("cropdust_pre")
            inst.AnimState:PushAnimation("cropdust_loop")
            inst.AnimState:PushAnimation("cropdust_pst", false)
        end,
        
        timeline=
        {
            TimeEvent(20*FRAMES, function(inst) 
                inst:PerformBufferedAction() 
                inst.sg:RemoveStateTag("busy") 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/items/weapon/bugrepellant")
            end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },

        onexit = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
        end,
    },
	    State{
        name = "investigate_start",
        tags = {"preinvestigate", "investigating", "working"},

        onenter = function(inst)
            if inst.components.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end
            inst.components.locomotor:Stop()
            inst.sg:GoToState("investigate")
        end,

        onexit = function(inst)
            if inst.components.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
        end,
        
        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end ),
            EventHandler("animover", function(inst) inst.sg:GoToState("investigate") end),
        },
    },

    State {
        name = "investigate",
        tags = {"preinvestigate", "investigating", "working"},

        onenter = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end
            inst.sg.statemem.action = inst:GetBufferedAction()
            inst.AnimState:PlayAnimation("lens")
        end,

        onexit = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
        end,
        
        timeline=
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.sg:RemoveStateTag("preinvestigate")
            end),


            TimeEvent(16*FRAMES, function(inst) 
                inst.sg:RemoveStateTag("investigating")
            end),

            TimeEvent(45*FRAMES, function(inst)
                -- this covers both mystery and lighting now
                inst:PerformBufferedAction()               
            end),
        },
        
        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end ),
            EventHandler("animover", function(inst)
                inst.sg:GoToState("investigate_post")
            end ),
        },
    },

    State{
        name = "investigate_post",
        tags = {"investigating", "working"},

        onenter = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end
            inst.AnimState:PlayAnimation("lens_pst")
        end,

        onexit = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
        end,
        
        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end ),
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State {
        name = "goggle",
        tags = {"preinvestigate", "investigating", "working"},

        onenter = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end
            inst.sg.statemem.action = inst:GetBufferedAction()
            inst.AnimState:PlayAnimation("goggle")
        end,

        onexit= function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
            inst.SoundEmitter:KillSound("goggle")                
        end, 
        
        timeline=
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.sg:RemoveStateTag("preinvestigate")
            end),

            TimeEvent(13*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_wagstaff/characters/wagstaff/use_goggles", "goggle")               
            end),            

            TimeEvent(16*FRAMES, function(inst) 
                inst.sg:RemoveStateTag("investigating")
            end),

            TimeEvent(45*FRAMES, function(inst)
                -- this covers both mystery and lighting now
                inst:PerformBufferedAction()               
            end),
        },
        
        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end ),
            EventHandler("animover", function(inst)
                inst.sg:GoToState("goggle_post")
            end ),
        },       
    },

    State{
        name = "goggle_post",
        tags = {"investigating", "working"},

        onenter = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetFourFaced()
            end
            inst.AnimState:PlayAnimation("goggle_pst")
        end,

        onexit = function(inst)
            if inst.replica.rider:IsRiding() then
                inst.Transform:SetSixFaced()
            end
        end,
        
        events=
        {
            EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end ),
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

for _, actionhandler in ipairs(actionhandlers) do
    AddStategraphActionHandler("wilson_client", actionhandler)
end

for _, state in ipairs(states) do
    AddStategraphState("wilson_client", state)
end

AddStategraphPostInit("wilson_client", function(sg)
	local _attack_onenter = sg.states.attack.onenter
	sg.states.attack.onenter = function(inst, data)
		local equip = inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
		if equip and equip:HasTag("halberd") then
			SetSoundAlias("dontstarve/wilson/attack_weapon", "dontstarve_DLC003/common/items/weapon/halberd")
		elseif equip and equip:HasTag("corkbat") then
			SetSoundAlias("dontstarve/wilson/attack_weapon", "dontstarve_DLC003/common/items/weapon/corkbat")
		end
		if equip and equip:HasTag("slowattack") then
			inst.sg.statemem.slowweapon = true
		end

		_attack_onenter(inst, data)
		
		if equip and equip:HasTag("slowattack") then
			inst.sg.statemem.slowweapon = true
			cooldown = math.max(cooldown, 23 * FRAMES)
			inst.sg:SetTimeout(cooldown)
		end
		
		SetSoundAlias("dontstarve/wilson/attack_weapon", nil)
	end
	--table.insert(sg.states.attack.timeline, TimeEvent(13*FRAMES, function(inst)
	--	if not inst.sg.statemem.slow and not inst.sg.statemem.slowweapon then
	--		inst.sg:RemoveStateTag("attack")
	--	end
	--end))
	--
    --table.insert(sg.states.attack.timeline, TimeEvent(23*FRAMES, function(inst)
	--	if inst.sg.statemem.slowweapon then
	--		inst.sg:RemoveStateTag("attack")
	--	end
	--end))
	local _attack_actionhandler = sg.actionhandlers[ACTIONS.ATTACK].deststate
	sg.actionhandlers[ACTIONS.ATTACK].deststate = function(inst, action, ...)
		if not (inst.sg:HasStateTag("attack") and action.target == inst.sg.statemem.attacktarget or inst.replica.health:IsDead()) then
			local equip = inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
			if equip and equip:HasTag("blunderbuss") then
				return "speargun"
			end
		end
		return _attack_actionhandler(inst, action, ...)
	end
end)
