return {
	ACTIONFAIL =
	{
        SHOP =
        {
        	GENERIC = "Can't buy",
        	CANTPAY = "Can't pay",
        },
        USEDOOR =
      	{
			GENERIC = "Won't open",
			LOCKED = "Nope. Locked",
      	},
	},
	ANNOUCE_UNDERLEAFCANOPY = "Hello, leaves!",
	ANNOUCE_ALARMOVER = "Whew! Safe again",
	ANNOUCE_BATS = "Ack! Flying Claws coming!",
	ANNOUCE_OTHERWORLD_DEED = "Not for here",
	ANNOUNCE_TOOLCORRODED = "Oh. It broke",
	ANNOUNCE_TURFTOOHARD = "Ugh. Ugh. Can't move it",
	ANNOUNCE_GAS_DAMAGE = "kack! kack!",

	ANNOUNCE_SNEEZE = "Hiccup!",	
	ANNOUNCE_HAYFEVER = "Happy time is coming!",	
	ANNOUNCE_HAYFEVER_OFF = "Oh. Happy time gone",	
	ANNOUNCE_PICKPOOP = {"Wheee!","Yay!","Woohoo!"},
	ANNOUNCE_TOO_HUMID = {"%s is heavy", "%s too hot"},
	ANNOUNCE_DEHUMID = {"Cooler now"},
	ANNOUNCE_PUGALISK_INVULNERABLE = {"Nope. Not there", "Too hard", "Wrong spot"},
	ANNOUNCE_MYSTERY_FOUND = "Something there?",
	ANNOUNCE_MYSTERY_NOREWARD = "Nothing",
	ANNOUNCE_MYSTERY_DOOR_FOUND = "Found wall hole",
    ANNOUNCE_MYSTERY_DOOR_NOT_FOUND = "Nope. No wall hole",
    ANNOUNCE_TAXDAY = "Pay day!",
    ANNOUNCE_HOUSE_DOOR = "Need paper first",
    ANNOUNCE_ROOM_STUCK = "Nope",
    ANNOUNCE_NOTHING_FOUND = "Nothing here",
	ANNOUNCE_SUITUP = "YAY!",
	DESCRIBE =
	{
		
		
		VINE = "String branches",
		BAMBOO = "Greenstick",
		FABRIC = "Soft. Made from friends",
		ARMOR_SNAKESKIN = "Poor Squirmy",
		SNAKESKIN = "Squirmy clothes",
		SNAKESKINHAT = "Snazzy Squirmy head thing",
		SNAKESKINSAIL = "Squirmy Wind Nabber",
		TURF_SNAKESKINFLOOR = "Not dirt",
		SNAKEOIL = "Hmmm...",
        SNAKE_AMPHIBIOUS = "Squirmy",
		BOAT_TORCH = "For dark times on floater",
		CARGOBOAT = "Big Floater of friends",
		ROWBOAT = "Push Pull Floater",
		BOATREPAIRKIT = "Fix it stuff",
		LOGRAFT = "Friend ship",
		VENOMGLAND = "For sickness",


		PIG_PALACE = "Pretty!",
		PIGMAN_QUEEN = "Fancy Twirly Tail",
        PIG_SCEPTER = "Makes friends with Twirly Tails!",
        PIGCROWNHAT = "Twirly Tail head thing",
		RECONSTRUCTION_PROJECT =
        {
            SCAFFOLD = "Making things",
            RUBBLE = "Broken",
        },
		PIG_SHOP_FLORIST =
		{
        	GENERIC = "Has dirt things",
        	BURNING = "NOOOOO!",
		},
        PIG_SHOP_GENERAL =
        {
        	GENERIC = "Sells stuff",
        	BURNING = "(sob)",
		},
        PIG_SHOP_HOOFSPA =
        {
        	GENERIC = "Boo Boo Place",
        	BURNING = "Help! Help!",
		},
        PIG_SHOP_PRODUCE =
        {
        	GENERIC = "Belly stuff inside",
        	BURNING = "AGGHHH!",
		},
       	PIGMAN_BEAUTICIAN =
		{
			GENERIC = "Boo Boo Twirly Tail",
			SLEEPING = "Shh...",
		},
		PIGMAN_ROYALGUARD =
		{
			GENERIC = "Twirly Tail tough guy",
			SLEEPING = "Sleepy",
		},
		PIGMAN_COLLECTOR =
		{
			GENERIC = "Have odd stuff?",
			SLEEPING = "Night night",
		},
		PIGMAN_MAYOR =
		{
			GENERIC = "Your town?",
			SLEEPING = "Sweet dreams",
		},
		PIGMAN_BANKER =
		{
			GENERIC = "Likes buying stuff",
			SLEEPING = "Sleep tight",
		},
		PIGMAN_MINER =
		{
			GENERIC = "Likes rocks",
			SLEEPING = "Goodnight",
		},
		PIGMAN_MECHANIC =
		{
			GENERIC = "Fix things?",
			SLEEPING = "Sleepy twirly tail...",
		},
		PIGMAN_FARMER =
		{
			GENERIC = "Likes grass friends",
			SLEEPING = "Shh... sleeping",
		},
		PIGMAN_FLORIST =
		{
			GENERIC = "Have dirt things?",
			SLEEPING = "Why not in bed?",
		},
		PIGMAN_STOREOWNER =
		{
			GENERIC = "Likes friend hair",
			SLEEPING = "So tired",
		},
		PIGMAN_ERUDITE =
		{
			GENERIC = "Odd Twirly Tail",
			SLEEPING = "Snoring Twirly Tail",
		},
		PIGMAN_HATMAKER =
		{
			GENERIC = "Have head stuff?",
			SLEEPING = "Tired",
		},
		PIGMAN_PROFESSOR =
		{
			GENERIC = "Wise old Twirly Tail",
			SLEEPING = "Sleepytime",
		},
		PIGMAN_HUNTER =
		{
			GENERIC = "Wants sharp things",
			SLEEPING = "Bedtime",
		},
        PIG_SHOP_ARCANE =
        {
        	GENERIC = "Magics place",
        	BURNING = "Oh no!!!",
        },
        PIG_SHOP_WEAPONS =
        {
        	GENERIC = "Stuff for fighting",
        	BURNING = "Friends are burning!",
        },
        PIG_SHOP_HATSHOP =
        {
        	GENERIC = "Has head things",
        	BURNING = "Save the head things!",
        },
        PIG_SHOP_ACADEMY =
        {
        	GENERIC = "Old stuff place",
        	BURNING = "Stay away! Stay away!",
        },
        TREE_PILLAR = "Big friend?",
        PIGHOUSE_CITY =
        {
        	GENERIC = "Twirly tail home",
        	BURNING = "Fire not good",
		},
        PIGHOUSE_MINE =
        {
        	GENERIC = "Twirly Tail home",
        	BURNING = "Bad fire! Bad!",
		},
        PIGHOUSE_FARM =
        {
        	GENERIC = "Home of dirt",
        	BURNING = "Save it! Save it!",
		},
        PIG_GUARD_TOWER =
        {
        	GENERIC = "For Twirly Tails",
        	BURNING = "Stay back, fire!",
		},
        CITY_LAMP =
        {
        	GENERIC = "Big light stick",
        	ON = "No light", --off
    	},
        DUNGBEETLE =
        {
        	GENERIC = "You like poop too?!",
        	UNDUNGED = "Where's poop?",
        	SLEEPING = "Shhh...",
        	DEAD = "Poor poop rider",
        },
        DUNGBALL = "Yay! Ball of poop!",
        DUNGPILE =
        {
        	GENERIC = "Poop!",
        	PICKED = "Aw. No more poop",
        },
        PIGEON =
        {
        	GENERIC = "Gray Tweeter",
        	SLEEPING = "Shh...",
        	DEAD = "(sob)",
        },
        SPIDER_MONKEY_TREE = "Trapped friend?",
        SPIDER_MONKEY =
        {
        	GENERIC = "Leggy Bug? or Cheeky?",
        	SLEEPING = "Shh... Don't wake him",
        	DEAD = "Oh dear",
        },
        SPIDER_MONKEY_NEST = "Anyone home?",
        TOPIARY = "You look nice",
        LAWNORNAMENT =
        {
        	GENERIC = "Want to be friends?",
        	BURNING = "Save friend! Save friend!",
        	BURNT = "(sob) No more",
        },
        HEDGE =
        {
        	GENERIC = "How are you?",
        	SHAVEABLE = "Needs hair cut",
        	BURNING = "AGGGHHH!",
        	BURNT = "(sob)",
        },
        RAINFORESTTREE =
        {
        	GENERIC = "Pretty hair, friend!",
        	CHOPPED = "Sorry. Didn't want to",
        	BURNING = "Stay away, fire!",
        	BURNT = "(sob)",
        },
        FLOWER_RAINFOREST = "Dirt made a friend",
        CHICKEN =
        {
        	GENERIC = "Ba-gawk!",
        	SLEEPING = "Shh...",
        	DEAD = "So sad",
        },
        GRASS_TALL =
        {
        	GENERIC = "You're so tall!",
        	PICKED = "Keep growing!",
        	BURNING = "No!!",
        },
        GLOWFLY =
        {
        	GENERIC = "Bzzter with bright peepers",        	
        	SLEEPING = "No light",
        	DEAD = "(sob)",
        },
        GLOWFLY_COCOON = "Changing",
        CHITIN = "Bzzter clothes",
        HANGING_VINE = "Wants stuff",
        GRABBING_VINE = "Wants stuff",
        VENUS_STALK = "Mmmm...",
        WALKINGSTICK = "Wheee!",
        ADULT_FLYTRAP =
        {
        	GENERIC = "Wants belly stuff",
        	SLEEPING = "Tired. Shh...",
        	DEAD = "(sob)",
        },
        MEAN_FLYTRAP =
        {
        	GENERIC = "Hungry?",
        	SLEEPING = "Tired. Shh...",
        	DEAD = "(sob)",
        },
        SNAPDRAGON =
        {
        	GENERIC = "Big belly stuff?",
        	SLEEPING = "Tired",
        	DEAD = "Oh dear",
        },
        LILYPAD = "Nice day for swim?",

        ZEB =
        {
        	GENERIC = "Neigh!",
        	SLEEPING = "Hard day",
        	DEAD = "Aw...",
        },
        SCORPION =
        {
        	GENERIC = "Stabby bzzter",
        	SLEEPING = "Nighty night",
        	DEAD = "No more stabby",
        },
        PEAGAWK =
        {
        	GENERIC = "Gubble Gubble Tweeter",
        	SLEEPING = "Sleeping?",
        	DEAD = "Oh dear",
        },
        PEEKHEN =
        {
        	GENERIC = "Gubble Gubble Tweeter", 
        	SLEEPING = "Sleeping",
        	DEAD = "Oh dear",
        },
        PEAGAWKFEATHERHAT = "Gubble Gubble!\nGubble Gubble!",
        PEAGAWK_BUSH = "Looking good!",
        PEAGAWKFEATHER = "Gubble Gubble Tweeter clothes",
        PEAGAWKFEATHER_PRISM = "Oooo...",

        ANTMAN =
        {
        	GENERIC = "Chr'ik?",
        	SLEEPING = "Sleep well",
        	DEAD = "(sniff) Too bad",
        },
        ANTHILL = "Anyone home?",
        ANTMASKHAT = "Chr'ik clothes",
        ANTSUIT = "Makes Chr'ik friends",
        
        PHEROMONESTONE = "Smells like Chr'ik",
        PIKO =
        {
        	GENERIC = "Fluffy Nabber",
        	SLEEPING = "Sleeping. Shh...",
        	DEAD = "Aw...",
        },
		RELIC_1 = {
        	GENERIC = "Happy rock!",
        	SUNKEN = "Swimming",
        },
        RELIC_2 = {
        	GENERIC = "Old Twirly Tail thing",
        	SUNKEN = "Something hiding?",
        },
        RELIC_3 = {
         	GENERIC = "Lost sniffer?",
         	SUNKEN = "See you!",
        },
        RELIC_4 = "Ohh... pretty rock",
      	RELIC_5 = "Fancy old thing",

        REEDS_WATER = "Good day, friend!",
        LOTUS = "Hello! How's water?",
		LOTUS_FLOWER = "Smells sweet!",
        LOTUS_FLOWER_COOKED ="Holey belly stuff",

        VAMPIREBAT =
        {
        	GENERIC = "Flying claws",
        	SLEEPING = "Sleeping down side up",
        	DEAD = "Oh",
        },
        VAMPIREBATCAVE = "Smells good!",

        FROGLEGS_POISON = "(sniff, sniff) Smells bad",
        FROGLEGS_POISON_COOKED = "(sniff, sniff) Smells okay",
		FROG_POISON =
		{
        	GENERIC = "Sick Ribbit",
        	SLEEPING = "Tired of ribbiting",
        	DEAD = "Too bad",
        },
        PIG_RUINS_TORCH_WALL = "Burns stick friends",
        PIG_RUINS_TORCH = "Burns stick friends",
        PIG_RUINS_HEAD = "Must have big rock brain",
        PIG_RUINS_ARTICHOKE = "Friend? Nope, rock",
        HALBERD = "Twirly Tail Fight Stick",
        PIG_RUINS_ENTRANCE =
        {
        	GENERIC = "Dark in there",
        	LOCKED = "Friends are in the way",
        },
        PIG_RUINS_EXIT = "Out",
        PIG_COIN = "Twirly Tails like this",

        OINC = "Twirly Tails like this",
        OINC10 = "Can buy things now",
        OINC100 = "Wow!!",

        RABID_BEETLE =
		{
        	GENERIC = "Why so mad, Bzzter?",
        	SLEEPING = "Let it sleep",
        	DEAD = "Too bad",
        },
        PARROT_BLUE =
		{
        	GENERIC = "Blue Tweeter",
        	SLEEPING = "Resting",
        	DEAD = "Dead",
        },
		KINGFISHER =
		{
        	GENERIC = "Poke Nose Tweeter",
        	SLEEPING = "Nighty night",
        	DEAD = "Sad. So sad",
        },
        BURR = "Cute!",   
        BURR_SAPLING =
		{
			BURNING = "NOOOO!",
			WITHERED = "Sick?",
			GENERIC = "Sleeping friend",
			PICKED = "Keep growing",
		},
		PIG_LATIN_1 = "Wants fire", -- light fires in pig latin

		DECO_RUINS_BEAM_ROOM ="Hard",
		DECO_CAVE_BEAM_ROOM = "Hard",
        DECO_CAVE_BAT_BURROW = "Hard",        		

        SMASHINGPOT = "Smash! Smash!",

        HIPPOPOTAMOOSE ="Big Water Hoppy",
		PIGGHOST = "Floaty Twirly Tail",        

        ANTCOMBHOME = "Little baby Chr'ik home",        
        SECURITYCONTRACT = "For a Twirly Tail friend",
        
        PLAYERHOUSE_CITY = {
        	BURNT = "AGGGGHH!",
        	FORSALE = "Empty",
        	SOLD = "Home"
        },
        JELLYBUG = "Jumpies",
        JELLYBUG_COOKED = "Why not jumping?",
        SLUGBUG = "Slimies",
        SLUGBUG_COOKED = "Hello, teeny sticks!",

        PLAYER_HOUSE_COTTAGE_CRAFT = "Makes home made of friends",
        PLAYER_HOUSE_VILLA_CRAFT = "Makes big home",           
		PLAYER_HOUSE_TUDOR_CRAFT = "Friends to build home",
        PLAYER_HOUSE_GOTHIC_CRAFT = "Builds home",           
		PLAYER_HOUSE_TURRET_CRAFT = "Builds pointy top home",
        PLAYER_HOUSE_BRICK_CRAFT = "Rocks for home",
		PLAYER_HOUSE_MANOR_CRAFT = "Makes REALLY big home",        

        CLIPPINGS = "Can sell this hair",

        CUTNETTLE = "(sniiiiiff)",
        NETTLE =
        {
        	WITHERED = "Not wet enough",
        	MOIST = "Growing good",
        	EMPTY = "Grow back",
        	DEFAULT = "Hello! Doing good?",
        },
    	DECO_RUINS_FOUNTAIN = "Wishy water",
        PUGALISK = "Big Wiggler",        
        SNAKE_BONE = "Ouch! Has sharp bits", 
        SNAKEBONESOUP = "Squirmy water", 

        BANDITMAP = "Where's stuff?", 
        BANDITTREASURE = "Stuff!", 

        BLUNDERBUSS = "Boom Stick!", 

        PUGALISK_FOUNTAIN = "Life water", 
        PUGALISK_RUINS_PILLAR = "Hard", 
        PUGALISK_TRAP_DOOR = "Hmm...What's here?",  

        TEA = "Warm Zippy water",      
        TEATREE = "Let them sleep",    
        TEATREE_SAPLING = "Hello, little friend",    
        TEATREE_NUT = "Want in belly? Or dirt?",    

        WALL_PIG_RUINS = "Wall", 

        PIG_RUINS_DART_TRAP = "Patuey!", 
        PIG_RUINS_SPEAR_TRAP = "Hello, sticks!", 
        PIG_RUINS_SPEAR_TRAP_TRIGGERED = "Oh!", 
        PIG_RUINS_SPEAR_TRAP_BROKEN = "Sorry, sticks", 
        PIG_RUINS_PRESSURE_PLATE = "Hmm...Odd rock", 
        PIG_RUINS_DART_STATUE = "Patuey!",                                          
        
        DISARMING_KIT = "Things for tricky walls", 

        BALLPEIN_HAMMER = "Teeny Smasher",

        GOLD_DUST = "Shiny dirt",

        ALOE = "Clumpy friend", 
        ALOE_COOKED = "Squishy", 
        ALOE_PLANTED = "Having fun in dirt",

        ASPARAGUS = "Sticks for belly", 
        ASPARAGUS_COOKED = "Smells pointy", 
        ASPARAGUS_PLANTED = "Hello there!", 

        RADISH = "Red balls for belly", 
        RADISH_COOKED = "Where green part go?",
        RADISH_PLANTED = "See you hiding!", 

        SNAKE_AMPHIBIOUS = "Squirmy",

        GASMASKHAT = "Breather with Tweeter clothes",
        PITHHAT = "Zoomy Head Thing", 
        
        BILL =
        {
        	GENERIC = "Prick beast",
        	SLEEPING = "Don't wake it",
        	DEAD = "Oh. Oh dear",
        },
        ROCK_ANTCAVE = "Rock. Looks hard",
        ANT_CAVE_LANTERN = "Sticky light",
        PROP_DOOR = "Where to?",

        PIKO_ORANGE = "Fluffy Nabber",

        TURF_PIGRUINS = "Not dirt",
        TURF_RAINFOREST = "Dirt",
        TURF_DEEPRAINFOREST = "Dirt",
        TURF_LAWN = "Dirt",
        TURF_GASJUNGLE = "Dirt",
        TURF_MOSS = "Soft and squishy",
        TURF_FIELDS = "Dirt",
        TURF_FOUNDATION = "Not dirt",
        TURF_COBBLEROAD = "Not dirt",

        MANDRAKEMAN = "Screamy",
        MANDRAKEHOUSE = "Hello? Anyone?",

         -- HOME DECO ITEMS
        INTERIOR_FLOOR_MARBLE = "Not dirt",
        INTERIOR_FLOOR_CHECK = "Not dirt",
        INTERIOR_FLOOR_PLAID_TILE = "Not dirt",
        INTERIOR_FLOOR_SHEET_METAL = "Not dirt",
        INTERIOR_FLOOR_WOOD = "Friends for floor",
        INTERIOR_FLOOR_GARDENSTONE = "Little bit of dirt",        
        INTERIOR_FLOOR_GEOMETRICTILES = "Not dirt",
        INTERIOR_FLOOR_SHAG_CARPET = "Not dirt",
        INTERIOR_FLOOR_TRANSITIONAL = "Peeking friends!",
        INTERIOR_FLOOR_WOODPANELS = "Friends for floor!",
        INTERIOR_FLOOR_HERRINGBONE = "Not dirt",
        INTERIOR_FLOOR_HEXAGON = "Not dirt",
        INTERIOR_FLOOR_HOOF_CURVY = "Not dirt",
        INTERIOR_FLOOR_OCTAGON = "Not dirt",

        INTERIOR_WALL_WOOD = "Friends for wall",
        INTERIOR_WALL_CHECKERED = "Wall",
        INTERIOR_WALL_FLORAL = "Friends?",
        INTERIOR_WALL_SUNFLOWER = "Friends?",
        INTERIOR_WALL_HARLEQUIN = "Wall",
        INTERIOR_WALL_PEAGAWK = "Tweeter wall",
        INTERIOR_WALL_PLAIN_DS = "Wall",
        INTERIOR_WALL_PLAIN_ROG = "Wall",
        INTERIOR_WALL_ROPE = "Wall",
        INTERIOR_WALL_CIRCLES = "Wall",
        INTERIOR_WALL_MARBLE = "Wall",
        INTERIOR_WALL_MAYORSOFFICE = "Wall",
        INTERIOR_WALL_FULLWALL_MOULDING = "Wall",
        INTERIOR_WALL_UPHOLSTERED = "Wall",   

        DECO_CHAIR_CLASSIC = "Sitting thing",
        DECO_CHAIR_CORNER = "Sitting thing",
        DECO_CHAIR_BENCH = "Sitting thing",
        DECO_CHAIR_HORNED = "Sitting thing",
        DECO_CHAIR_FOOTREST = "Foot thing",
        DECO_CHAIR_LOUNGE = "Sitting thing",
        DECO_CHAIR_MASSAGER = "Sitting thing?",
        DECO_CHAIR_STUFFED = "Sitting thing",
        DECO_CHAIR_ROCKING = "Sitting thing",
        DECO_CHAIR_OTTOMAN = "Foot thing",

        DECO_LAMP_FRINGE = "Light",
        DECO_LAMP_STAINGLASS = "Light",
        DECO_LAMP_DOWNBRIDGE = "Light",
        DECO_LAMP_2EMBROIDERED = "Light",
        DECO_LAMP_CERAMIC = "Light",
        DECO_LAMP_GLASS = "Light",
        DECO_LAMP_2FRINGES = "Light",
        DECO_LAMP_CANDELABRA = "Light",
        DECO_LAMP_ELIZABETHAN = "Light",
        DECO_LAMP_GOTHIC = "Light",
        DECO_LAMP_ORB = "Light",
        DECO_LAMP_BELLSHADE = "Light",
        DECO_LAMP_CRYSTALS = "Light",
        DECO_LAMP_UPTURN = "Light",
        DECO_LAMP_2UPTURNS = "Light",
        DECO_LAMP_SPOOL = "Light",
        DECO_LAMP_EDISON = "Light",
        DECO_LAMP_ADJUSTABLE = "Light",
        DECO_LAMP_RIGHTANGLES = "Light",
        DECO_LAMP_HOOFSPA = "Twirly Tail light",

        DECO_CHAISE = "Sitting thing",

        DECO_PLANTHOLDER_BASIC = "Friend?",
        DECO_PLANTHOLDER_WIP = "Need help?",
        DECO_PLANTHOLDER_FANCY = "Hello!",
        DECO_PLANTHOLDER_BONSAI = "You look nice",
        DECO_PLANTHOLDER_DISHGARDEN = "Small friends",
        DECO_PLANTHOLDER_PHILODENDRON = "Neat friend",
        DECO_PLANTHOLDER_ORCHID = "Nice friend",
        DECO_PLANTHOLDER_DRACEANA = "Hi there!",
        DECO_PLANTHOLDER_XEROGRAPHICA = "How are you?",
        DECO_PLANTHOLDER_BIRDCAGE = "Trapped?",
        DECO_PLANTHOLDER_PALM = "Hello",
        DECO_PLANTHOLDER_ZZ = "Nice hair!",
        DECO_PLANTHOLDER_FERNSTAND = "Friend!",
        DECO_PLANTHOLDER_FERN = "Hello up there!",
        DECO_PLANTHOLDER_TERRARIUM = "Anyone home?",
        DECO_PLANTHOLDER_PLANTPET = "Sit! Stay",
        DECO_PLANTHOLDER_TRAPS = "Cute!",
        DECO_PLANTHOLDER_PITCHERS = "Hanging around?",
        DECO_PLANTHOLDER_MARBLE = "Friend",

        DECO_PLANTHOLDER_WINTERFEASTTREEOFSADNESS = "Okay, friend?",  
        DECO_PLANTHOLDER_WINTERFEASTTREE = "Nice to see a friend!",

        DECO_TABLE_ROUND = "Flat thing",
        DECO_TABLE_BANKER = "Flat thing",
        DECO_TABLE_DIY = "Knew that tree",
        DECO_TABLE_RAW = "Friends?",
        DECO_TABLE_CRATE = "Friends made a box",
        DECO_TABLE_CHESS = "Game?",

        DECO_ANTIQUITIES_WALLFISH = "Oh",        
        DECO_ANTIQUITIES_BEEFALO = "Oh",
        DECO_WALLORNAMENT_PHOTO = "Hmm...",
        DECO_WALLORNAMENT_FULLLENGTH_MIRROR = "Friend?",
        DECO_WALLORNAMENT_EMBROIDERY_HOOP = "Hmm...",
        DECO_WALLORNAMENT_MOSAIC = "Hmm...",
        DECO_WALLORNAMENT_WREATH = "Circle of friends",
        DECO_WALLORNAMENT_AXE = "Yikes!",
        DECO_WALLORNAMENT_HUNT = "Pointy sticks",
        DECO_WALLORNAMENT_PERIODIC_TABLE = "Hmm...",
        DECO_WALLORNAMENT_GEARS_ART = "Hmm...",
        DECO_WALLORNAMENT_CAPE = "Hmm...",
        DECO_WALLORNAMENT_NO_SMOKING = "Hmm...",
        DECO_WALLORNAMENT_BLACK_CAT = "Mow? Mow?",

        WINDOW_ROUND_CURTAINS_NAILS = "Outside",
        WINDOW_ROUND_BURLAP = "Outside",
        WINDOW_SMALL_PEAKED = "Outside",
        WINDOW_LARGE_SQUARE = "Outside",
        WINDOW_TALL = "Outside",
        WINDOW_LARGE_SQUARE_CURTAIN = "Outside",
        WINDOW_TALL_CURTAIN = "Outside",
        WINDOW_SMALL_PEAKED_CURTAIN = "Outside",
        WINDOW_GREENHOUSE = "Can see lots of outside!",
        WINDOW_ROUND = "Outside",

        DECO_WOOD_CORNERBEAM = "Friend?",
        DECO_MARBLE_CORNERBEAM = "Hard",
        DECO_WOOD       = "Hello?",
        DECO_MILLINERY  = "Friend?",
        DECO_ROUND      = "Hard",
        DECO_MARBLE     = "Hard",
        DECO_RUINS_BEAM_ROOM_BLUE = "Hard",

        SWINGING_LIGHT_BASIC_BULB = "Light",
        SWINGING_LIGHT_FLORAL_BLOOMER = "Light friend?",
        SWINGING_LIGHT_CHANDALIER_CANDLES = "Little fires",
        SWINGING_LIGHT_ROPE_1 = "Light",
        SWINGING_LIGHT_ROPE_2 = "Light",
        SWINGING_LIGHT_FLORAL_BULB = "Light",
        SWINGING_LIGHT_PENDANT_CHERRIES = "Light",
        SWINGING_LIGHT_FLORAL_SCALLOP = "Light",
        SWINGING_LIGHT_FLORAL_BLOOMER = "Light",
        SWINGING_LIGHT_BASIC_METAL = "Light",
        SWINGING_LIGHT_TOPHAT = "Light made of a Head Thing",
        SWINGING_LIGHT_DERBY = "Light made of a Head Thing",
        SWINGING_LIGHT1 = "Light!",

        RUG_ROUND = "Ground thing",
        RUG_SQUARE = "Ground thing",
        RUG_OVAL = "Ground thing",
        RUG_RECTANGLE = "Ground thing",
        RUG_FUR = "Aww...",
        RUG_HEDGEHOG = "Ground thing",
        RUG_PORCUPUSS = "Friend? Friend?",
        RUG_HOOFPRINT = "Ground thing",
        RUG_OCTAGON = "Ground thing",
        RUG_SWIRL = "Ground thing",
        RUG_CATCOON = "Oh",
        RUG_RUBBERMAT = "Ground thing",
        RUG_WEB = "Sticky",
        RUG_METAL = "Ground thing",
        RUG_WORMHOLE = "Ground thing",
        RUG_BRAID = "Ground thing",
        RUG_BEARD = "Ground thing",
        RUG_NAILBED = "Pokey",
        RUG_CRIME = "Ground thing",
        RUG_TILES = "Ground thing",

        SHELVES_WOOD = "Helping friends",
        SHELVES_CINDERBLOCKS = "For stuff",
        SHELVES_MARBLE = "For stuff",
        SHELVES_MIDCENTURY = "For stuff",
        SHELVES_GLASS = "For stuff",
        SHELVES_LADDER = "For stuff",
        SHELVES_HUTCH = "For stuff",
        SHELVES_INDUSTRIAL = "For stuff",
        SHELVES_ADJUSTABLE = "For stuff",
        SHELVES_WALLMOUNT = "For stuff",
        SHELVES_AFRAME = "For stuff",
        SHELVES_CRATES = "For stuff",
        SHELVES_FRIDGE = "For stuff",
        SHELVES_HOOKS = "For stuff",
        SHELVES_PIPE = "For stuff",
        SHELVES_HATTREE = "For stuff",
        SHELVES_PALLET = "For stuff",
        SHELVES_BASIC = "For stuff",
        SHELVES_FLOATING = "For stuff",
        SHELVES_METAL = "For stuff",

        WOOD_DOOR = "Door of friends",
		STONE_DOOR = "Another room",
		ORGANIC_DOOR = "Hello? Welcome",
		IRON_DOOR = "Twirly door",
		PILLAR_DOOR = "Hello! Hello?",
		CURTAIN_DOOR = "Another room",
		ROUND_DOOR = "Big mouth door",
		PLATE_DOOR = "Another room",

        ROCK_FLIPPABLE = "Things hiding?",

        PLAYER_HOUSE_COTTAGE = "Home made of friends",
        PLAYER_HOUSE_VILLA = "Big home",  
        PLAYER_HOUSE_TUDOR = "Big home",
        PLAYER_HOUSE_MANOR = "Really big home",                
        PLAYER_HOUSE_GOTHIC = "Home",                
        PLAYER_HOUSE_BRICK = "Rock home",   
        PLAYER_HOUSE_TURRET = "Pointy top home",
                     
        BRAMBLESPIKE = "Wheeee!", 

        SUNKEN_RELIC = "Swimming?",  

        IRON = "Clinky Rock", 
        
        CLAWPALMTREE = "Friend?",     
        DUG_NETTLE = "Needs dirt",

        THUNDERBIRD = "Zzzt Tweeter",
        THUNDERBIRDNEST = "Zzzt Tweeter home",
        FEATHER_THUNDER = "Zzzt Tweeter clothes", 
        THUNDERHAT = "Pointy Head Thing",

        WEEVOLE = "Wee Bzzter",
        WEEVOLE_CARAPACE = "Crunchy",
        ARMOR_WEEVOLE = "Bzzter clothes",
        
        TUBERTREE = "Odd friend",
        CORK = "Wood? Not wood", 

        CANDLEHAT = "Scary head thing!",
        CORK_BAT = "Puny Whacker", 

        LEATHER = "Oh",
        BAT_HIDE = "Bum. Twirly Tail bum?", 

        ANCIENT_ROBOT_RIBS = "Piece of Clinky Rock Man",
        ANCIENT_ROBOT_CLAW = "Shake?",
        ANCIENT_ROBOT_LEG = "Clinky Rock Walker",    
        ANCIENT_ROBOT_HEAD = "Full of Clinky Rock",

        TEATREE_NUT_COOKED = "Not for dirt now",

        DEED = "For home",
        CONSTRUCTION_PERMIT = "Build rooms?",
        DEMOLITION_PERMIT = "Break rooms?",

		CLAWPALMTREE_SAPLING = "Grow friend! Grow!", 

        BANDITHAT = "Sneaky Head Thing",
        PIGBANDIT = "Sneaky Twirly Tail",

        PIG_RUINS_CREEPING_VINES = "Friends?",

        MAGNIFYING_GLASS = "Peeper",

        CORKCHEST = "Put stuff inside",
        CORKBOAT = "Puny Floater",

        SEDIMENTPUDDLE = "Shiny puddle",
        GOLDPAN = "Gets Shiny from puddle",

        PORKLAND_ENTRANCE = "Where to?",
        PORKLAND_EXIT = "Where to?",

        POG =
        {
        	GENERIC = "Bouncy Woofer",
        	FOLLOWER = "Friend",
        	SLEEPING = "Sleepy Snoozer",
        	APORKALYPSE = "Bad Woofer",
        },
        PANGOLDEN = "Shiny Pooper",

        ROC_LEG = "Wump Tweeter",
        ROC_HEAD = "Hungry?",
        ROC_TAIL = "Wump Tweeter bum",

        GNATMOUND = "Hello?",
        GNAT = "Stay away, Bzzters!",
        
        PORKLAND_INTRO = "Where going?",

        ROC_NEST_TREE1 = "Friend?",
        ROC_NEST_TREE2 = "Hello!",
        ROC_NEST_BUSH = "Like your hair",
        ROC_NEST_BRANCH1 = "Lost?",
        ROC_NEST_BRANCH2 = "Lost?",
        ROC_NEST_TRUNK = "You alright?",
        ROC_NEST_HOUSE = "No one there",
        ROC_NEST_RUSTY_LAMP = "Working?",

        ROC_NEST_EGG1 = "Broken",
        ROC_NEST_EGG2 = "Broken",
        ROC_NEST_EGG3 = "Broken",
        ROC_NEST_EGG4 = "Broken",

        ROC_ROBIN_EGG = "Needs warm",

        TUBER_CROP = "Not ready",
        TUBER_BLOOM_CROP = "Ready!",
        TUBER_CROP_COOKED = "Good now",
        TUBER_BLOOM_CROP_COOKED = "Hello flower friend!",

        ALLOY = "Bar of Clink Rock",

        ARMOR_METALPLATE = "Clink Rock clothes",
        METALPLATEHAT = "Clink Rock Head Thing",

        SMELTER = "Hot! Hot!",

        BUGREPELLENT = "Smells bad",
        
        HOGUSPORKUSATOR = "Twirly Tail Tweeter?",

        GASCLOUD = "Bad air",

        SHEARS = "For hair cuts",
    	BATHAT = "Woo Woo Head Thing",

    	WATERDROP = "For dirt",

    	LIFEPLANT = "Lively friend",

    	TRINKET_GIFTSHOP_1 = "Rock. Pretty rock",
    	TRINKET_GIFTSHOP_3 = "Ooohh... nice place",        

    	KEY_TO_CITY = "For Twirly Tail Town",

    	PEDESTAL_KEY = "Little key",

    	ROYAL_GALLERY = "Has little lock",

    	APORKALYPSE_CLOCK = "Tic Tock",
    	ANCIENT_HERALD = "Dark Floaty Man",

    	ASPARAGUSSOUP = "Swimming sticks",
    	SPICYVEGSTINGER = "Tangy water",
    	FEIJOADA = "Full of Jumpies",
    	HARDSHELL_TACOS = "Crunchy Bzzters",
    	GUMMY_CAKE = "Squishy",
    	STEAMEDHAMSANDWICH = "Egads!",
		MEATED_NETTLE = "Mmmm...",

    	ROC_NEST_DEBRIS1 = "Friend!",
    	ROC_NEST_DEBRIS2 = "Hello, stick!",
    	ROC_NEST_DEBRIS3 = "Friend?",
    	ROC_NEST_DEBRIS4 = "You live around here?",

    	ANTQUEEN = "Chr'ik Mommy",
      	ANTQUEEN_CHAMBERS = "Chr'ik Mommy room",
    	ANTQUEEN_THRONE = "Looks comfy",
    	ANTMAN_WARRIOR_EGG = "Little Baby Chr'ik. Hello!",
    	ANTMAN_WARRIOR = "Mad Chr'ik! Agh!",
 		ANTCHEST = "Buzz juice! Where's Buzz?",
 		NECTAR_POD = "Sweet!",

    	PIG_SHOP_DELI =
        {
            GENERIC = "Good belly stuff!",
            BURNING = "Don't like fire",
        },
        PIG_SHOP_CITYHALL =
        {
            GENERIC = "Papers inside",
            BURNING = "Nooooo!",
        },
        PIG_SHOP_CITYHALL_PLAYER =
         {
            GENERIC = "Work place",
            BURNING = "Stay back!",
        },
        PIG_GUARD_TOWER_PALACE =
        {
            GENERIC = "For Twirly Tail tough guys",   
            BURNING = "YAAAAAAHH!",
        },
        PIG_RUINS_PIG = "Twirly Tail rock",
        PIG_RUINS_IDOL = "Twirly Tail rock",
        PIG_RUINS_PLAQUE = "Twirly Tail rock",
       
        BASEFAN = "Wind",
        SPRINKLER = "Makes air water",

        NETTLELOSANGE = "Mmm...",

        ICEDTEA = "Cold Zippy water",

        TRINKET_GIFTSHOP_4 = "Twirly Tail Can",

        WALLCRACK_RUINS = "Broken",

        RAINFORESTTREE_ROT = "You sick, friend?",

        DEFLATED_BALLOON = "Broken",
		DEFLATED_BALLOON_BASKET = "Broken",

		SNAKE_FIRE = "Fire Squirmy!",

		DISGUISEHAT = "Twirly Tail?",

		ALOE_SEEDS = "Want some dirt?",
		ASPARAGUS_SEEDS = "Grows the belly sticks",
		RADISH_SEEDS = "You're going in dirt!",

		CAVE_EXIT_ROC = "Goes away from here",

        PORKLAND_INTRO_BASKET = "Fell from sky",
        PORKLAND_INTRO_BALLOON = "Went pbblt",
        PORKLAND_INTRO_TRUNK = "Something inside? Nope",
        PORKLAND_INTRO_SUITCASE = "Going on trip?",
        PORKLAND_INTRO_FLAGS = "Pretty string",
        PORKLAND_INTRO_SANDBAG = "Dirt bag exploded",

        TURF_BEARD_HAIR = "Not dirt", 

        HIPPO_ANTLER = "Big Head Branch",
        BILL_QUILL = "Pokey part",

        ANTLER = "Sounds like Womp Tweeter!",
        ANTLER_CORRUPTED = "Scares Womp Tweeter",

        PUGALISK_SKULL = "From big Wiggler",
        PUGALISK_CORPSE = "Growing bones?",

        BONESTAFF = "Stare Stick",

        CITY_HAMMER = "Town Smasher",

        TURF_PAINTED = "Dirt", 
		TURF_PLAINS = "Dirt",

		PIG_SHOP_BANK =
        {
          GENERIC = "Twirly Tail Shiny place",
          BURNING = "Oh",
        },
		PIGMAN_USHER =
        {
          GENERIC = "Good day, Twirly Tail",
          SLEEPING = "Shh...",
        },
        PIGMAN_ROYALGUARD_2 =
        {
          GENERIC = "Twirly Tail Tough Guy",
          SLEEPING = "Needs bed",
        },
        PIG_SHOP_ANTIQUITIES =
        {
          GENERIC = "Strange Things place",
          BURNING = "Fire! Fire!",
        },
        PIG_RUINS_ENTRANCE2 = "Dark in there",
        PIG_RUINS_EXIT2 = "Door to outside",
        PIG_RUINS_ENTRANCE3 = "What's in there?",   
        PIG_RUINS_ENTRANCE4 = "Where does that go?",           
        PIG_RUINS_EXIT4 = "Goes somewhere else",
        PIG_RUINS_ENTRANCE5 = "What's in there?",           
        PIG_RUINS_ENTRANCE_SMALL = "Dark in there",

        RO_BIN = "Pack Tweeter!",
        RO_BIN_GIZZARD_STONE = "Oh, hello!",
        GIANTGRUB = "Dirt wiggly!",

        DECO_RUINS_ENDSWELL = "Not good water. Not good",
		
		-------rewards update-------

		TURF_DEEPRAINFOREST_NOCANOPY = "Dirt. Good dirt",

		BRAMBLE_CORE = "Friend! Beautiful friend!",
        BRAMBLE_BULB = "Not ground stuff",
        ROOTTRUNK_CHILD = "Friends help carry stuff",

		PIG_SHOP_TINKER =
        {
            GENERIC = "For stuff making",
            BURNING = "Aggh!",
        },
        ARMORVORTEXCLOAK = "Dark Floaty clothes",    
        ANCIENT_REMNANT = "Feels cold", 

        GOGGLESNORMALHAT = "Hehe. Funny eyes",
        GOGGLESHEATHAT = "Ribbit? No",
        GOGGLESARMORHAT = "Clink eye thing",
        GOGGLESSHOOTHAT = "Can't use",     
        THUMPER = "Thump! Thump!",
        TELEBRELLA = "Not rain taker",
        TELIPAD = "Returny place",

        TRUSTY_SHOOTER = "Honk! Honk!",
        WHEELER_TRACKER = "Where to?",

        ANCIENT_HULK = "Clinky Rock Man is awake!",
        ROCK_BASALT = "Rock",
        LIVING_ARTIFACT = "Boom clothes!",
        INFUSED_IRON = "Oh... shiny",

        TELEPORTATO_HAMLET_BASE = "Stone",
		TELEPORTATO_HAMLET_BOX = "Box",
		TELEPORTATO_HAMLET_CRANK = "Lever",
		TELEPORTATO_HAMLET_POTATO = "Thing",
		TELEPORTATO_HAMLET_RING = "Ring",

		ANCIENT_ROBOTS_ASSEMBLY = "Not done",

		HEDGE_BLOCK_ITEM = "Where you want to live?",
        HEDGE_CONE_ITEM = "Nice haircut!",
        HEDGE_LAYERED_ITEM = "Fancy friend wall",
   	},
	DESCRIBE_MYSTERY = "What's that?",
	DESCRIBE_NEARSIGHTED = "Everything's air soupy!",
}